package test;

import static org.junit.Assert.assertEquals;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.xml.bind.JAXBException;

import org.junit.Test;
import org.w3c.xsd.owl2.Ontology;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import es.ull.iis.ontology.radios.json.schema4simulation.Schema4Simulation;
import es.ull.iis.ontology.radios.json.schema4simulation.builders.DiseaseBuilder;
import es.ull.iis.ontology.radios.json.schema4simulation.builders.OwlHelper;
import es.ull.iis.ontology.radios.utils.OntologyUtils;

public class TestsOwl2Json {
	 @Test
	public void generateJsonFileFromRaDiOS() throws JsonProcessingException {
		System.out.println("*************************************************************************************************************");

		boolean expectedResult = true;
		boolean result = true;

		try {
			String diseaseName = "#PBD_ProfoundBiotinidaseDeficiency";
			String radiosPath = "src/main/resources/owl/radios_PBD.owl";
			String filenameResult = "radios_PBD.json";
			generateJsonFile(diseaseName, radiosPath, filenameResult);

			diseaseName = "#SCD_Disease_SickleCell";
			radiosPath = "src/main/resources/owl/radios_SCD.owl";
			filenameResult = "radios_SCD.json";
			generateJsonFile(diseaseName, radiosPath, filenameResult);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}
		
		assertEquals(expectedResult, result);
		System.out.println("Test terminado ...");
		System.out.println("=============================================================================================================\n");
	}
	
	@Test
	public void generateJsonFileFromRaDiOSTests() throws JsonProcessingException {
		System.out.println("*************************************************************************************************************");

		boolean expectedResult = true;
		boolean result = true;

		try {
			// =================================================================================================
			// Input parameters
			// =================================================================================================
			String diseaseName = "#RD1_Disease";
			String radiosPath = "src/main/resources/owl/radios-test_disease1.owl";
			String filenameResult = "radios-test_disease1.json";
			generateJsonFile(diseaseName, radiosPath, filenameResult);

			diseaseName = "#RD2_Disease";
			radiosPath = "src/main/resources/owl/radios-test_disease2.owl";
			filenameResult = "radios-test_disease2.json";
			generateJsonFile(diseaseName, radiosPath, filenameResult);

			diseaseName = "#RD3_Disease";
			radiosPath = "src/main/resources/owl/radios-test_disease3.owl";
			filenameResult = "radios-test_disease3.json";
			generateJsonFile(diseaseName, radiosPath, filenameResult);

			diseaseName = "#RD4_Disease";
			radiosPath = "src/main/resources/owl/radios-test_disease4.owl";
			filenameResult = "radios-test_disease4.json";
			generateJsonFile(diseaseName, radiosPath, filenameResult);
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}
		
		assertEquals(expectedResult, result);
		System.out.println("Test terminado ...");
		System.out.println("=============================================================================================================\n");
	}

	private void generateJsonFile(String diseaseName, String radiosPath, String filenameResult) throws FileNotFoundException, JAXBException, IOException {
		Ontology radios = OntologyUtils.loadOntology(radiosPath);
		OwlHelper.initilize(radios);
		
		Schema4Simulation schema4Simulation = new Schema4Simulation();
		schema4Simulation.setDisease(DiseaseBuilder.getDisease(diseaseName));			
		
		ObjectMapper mapper = new ObjectMapper()
				.enable(SerializationFeature.INDENT_OUTPUT)
				.setSerializationInclusion(Include.NON_NULL)
				.setSerializationInclusion(Include.NON_EMPTY);
		
		FileOutputStream fos = new FileOutputStream(filenameResult);
		fos.write(mapper.writeValueAsString(schema4Simulation).getBytes());
		fos.close();

		FileOutputStream fosForPsighos = new FileOutputStream("/home/davidpg/workspace/java/sighos-radios/PSIGHOS-RaDiOS/resources/" + filenameResult);
		fosForPsighos.write(mapper.writeValueAsString(schema4Simulation).getBytes());
		fosForPsighos.close();
	}
	
}
 