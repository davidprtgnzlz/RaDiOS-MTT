package test;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;

import org.junit.Test;
import org.w3c.xsd.owl2.Axiom;
import org.w3c.xsd.owl2.ClassAssertion;
import org.w3c.xsd.owl2.DataPropertyAssertion;
import org.w3c.xsd.owl2.NamedIndividual;
import org.w3c.xsd.owl2.ObjectProperty;
import org.w3c.xsd.owl2.ObjectPropertyAssertion;
import org.w3c.xsd.owl2.Ontology;

import es.ull.iis.ontology.radios.Constants;
import es.ull.iis.ontology.radios.NodeData;
import es.ull.iis.ontology.radios.TreeNode;
import es.ull.iis.ontology.radios.json.databind.InputParametersDecisionTree;
import es.ull.iis.ontology.radios.service.DataStoreService;
import es.ull.iis.ontology.radios.service.DecisionTreeBuilderService;
import es.ull.iis.ontology.radios.utils.CostsUtils;
import es.ull.iis.ontology.radios.utils.OntologyUtils;
import es.ull.iis.ontology.radios.utils.TreeUtils;

public class Tests {
	private static String RADIOS_PATH_LOCATION = "src/main/resources/owl/radios.owl";

	// @Test
	public void loadOntology() {
		System.out.println("*************************************************************************************************************");

		boolean expectedResult = true;
		boolean result = true;

		Ontology radios;
		try {
			radios = OntologyUtils.loadOntology(RADIOS_PATH_LOCATION);
			if (radios == null) {
				result = false; 
			}
		} catch (JAXBException | IOException e) {
			e.printStackTrace();
		}

		assertEquals(expectedResult, result);
		System.out.println("Test terminado ...");
		System.out.println("=============================================================================================================\n");
	}

	// @Test
	public void loadDecisionTree() {
		System.out.println("*************************************************************************************************************");

		boolean expectedResult = true;
		boolean result = true;

		try {
			Ontology radios = OntologyUtils.loadOntology(RADIOS_PATH_LOCATION);

			// *********************** DATA INPUT ******************************
			InputParametersDecisionTree inputParametersDecisionTree = new InputParametersDecisionTree ();
			inputParametersDecisionTree.setDisease("#PBD_ProfoundBiotinidaseDeficiency");
			List<String> interventionsToCompare = new ArrayList<>();			
			interventionsToCompare.add("#PBD_InterventionNoScreening");
			interventionsToCompare.add("#PBD_InterventionScreening");
			inputParametersDecisionTree.setInterventions(interventionsToCompare);
			inputParametersDecisionTree.setClinicalDiagnosisStrategy("#PBD_ClinicalDiagnosisStrategy");
			inputParametersDecisionTree.setDiscountRateCosts(0.0);
			inputParametersDecisionTree.setDiscountRateEffects(0.0);
			inputParametersDecisionTree.setUtilityGeneralPopulation(0.8861);
			// *****************************************************************

			TreeNode<NodeData> decisionTree = DecisionTreeBuilderService.buildDecisionTreeFromOntology(radios, inputParametersDecisionTree);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			TreeUtils.inorden(decisionTree, Constants.CONSTANT_EMPTY_STRING, baos);
			System.out.println(new String(baos.toByteArray()));
			
			System.out.println();
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		assertEquals(expectedResult, result);
		System.out.println("Test terminado ...");
		System.out.println("=============================================================================================================\n");
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	// @Test
	public void recoverAllClassInstances() {
		System.out.println("*************************************************************************************************************");

		boolean expectedResult = true;
		boolean result = true;

		Map<String, List<String>> instancesByClazz = new HashMap<String, List<String>>();
		Map<String, String> instanceToClazz = new HashMap<String, String>();

		try {
			Ontology radios = OntologyUtils.loadOntology(RADIOS_PATH_LOCATION);
			
			TreeNode nArioTree = null;
			List<ClassAssertion> diseases = DataStoreService.eTLClassIndividuals(radios, instancesByClazz, instanceToClazz, "#Disease");
			for (ClassAssertion disease : diseases) {
				nArioTree = OntologyUtils.buildNArioTreeFromDisease(radios, instanceToClazz, disease);
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				TreeUtils.inorden(nArioTree, Constants.CONSTANT_EMPTY_STRING, baos);
				System.out.println(new String(baos.toByteArray()));
			}

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		assertEquals(expectedResult, result);
		System.out.println("Test terminado ...");
		System.out.println("=============================================================================================================\n");
	}

	@Test
	public void testShowAssertions() {
		System.out.println("*************************************************************************************************************");

		boolean expectedResult = true;
		boolean result = true;

		Map<String, List<String>> instancesByClazz = new HashMap<String, List<String>>();
		Map<String, String> instanceToClazz = new HashMap<String, String>();

		try {
			Ontology radios = OntologyUtils.loadOntology(RADIOS_PATH_LOCATION);
			DataStoreService.eTLClassIndividuals(radios, instancesByClazz, instanceToClazz, "#Disease");

			for (Axiom axiom : radios.getAxiom()) {
				if (axiom instanceof ObjectPropertyAssertion) {
					showObjectPropertyAssertionFields(instanceToClazz, (ObjectPropertyAssertion) axiom);
				} else if (axiom instanceof DataPropertyAssertion) {
					shoDataPropertyAssertionFields(instanceToClazz, (DataPropertyAssertion) axiom);
				}
			}
		} catch (Exception e) {
			result = false;
		}

		assertEquals(expectedResult, result);
		System.out.println("Test terminado ...");
		System.out.println("=============================================================================================================\n");
	}

	// @Test
	public void testCalculateStrategyCosts() {
		System.out.println("*************************************************************************************************************");

		boolean expectedResult = true;
		boolean result = true;

		try {
			Ontology radios = OntologyUtils.loadOntology(RADIOS_PATH_LOCATION);

			String strategy = null;
			
			strategy = "#PBD_ClinicalDiagnosisStrategy";
			System.out.println(String.format("Strategy [%s] - Cost = [%s]", strategy, CostsUtils.calculateStrategyCost(radios, strategy)));

			strategy = "#PBD_FollowUpStrategy";
			System.out.println(String.format("Strategy [%s] - Cost = [%s]", strategy, CostsUtils.calculateStrategyCost(radios, strategy)));

			strategy = "#PBD_ScreeningStrategy";
			System.out.println(String.format("Strategy [%s] - Cost = [%s]", strategy, CostsUtils.calculateStrategyCost(radios, strategy)));

			strategy = "#PBD_TreatmentStrategy";
			System.out.println(String.format("Strategy [%s] - Cost = [%s]", strategy, CostsUtils.calculateStrategyCost(radios, strategy)));

			Double followUpStrategyCost = CostsUtils.calculateStrategyCost(radios, "#PBD_FollowUpStrategy");
			Double treatmentStrategyCost = CostsUtils.calculateStrategyCost(radios, "#PBD_TreatmentStrategy");
			
			Double discontRate = 0.03;
			Double lifeExpectancy = 82.27;
			System.out.println(String.format("Value without discount: %.3f", (followUpStrategyCost + treatmentStrategyCost) * lifeExpectancy));
			System.out.println(String.format("Value with discount: %.3f", CostsUtils.calculateDiscount(followUpStrategyCost + treatmentStrategyCost, discontRate, 0.0, lifeExpectancy)));
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		assertEquals(expectedResult, result);
		System.out.println("Test terminado ...");
		System.out.println("=============================================================================================================\n");
	}

	// @Test
	public void testCalculateDiseaseInterventionStrategySensitivityAndSpecificity() {
		System.out.println("*************************************************************************************************************");

		boolean expectedResult = true;
		boolean result = true;

		try {
			Ontology ontology = OntologyUtils.loadOntology(RADIOS_PATH_LOCATION);

			String disease = "#PBD_ProfoundBiotinidaseDeficiency";
			String intervention = "#PBD_InterventionScreening";
			
			Double [] sensitivityAndSpecificity = OntologyUtils.calculateDiseaseStrategySensitivityAndSpecificity(ontology, disease, intervention, Constants.OBJECTPROPERTY_INTERVENTION_SCREENING_STRATEGY);
			System.out.println(String.format("Sensibility = %f, Specificity = %f", sensitivityAndSpecificity[0], sensitivityAndSpecificity[1]));

			sensitivityAndSpecificity = OntologyUtils.calculateDiseaseStrategySensitivityAndSpecificity(ontology, disease, intervention, Constants.OBJECTPROPERTY_INTERVENTION_CLINICAL_DIAGNOSIS_STRATEGY);
			System.out.println(String.format("Sensibility = %f, Specificity = %f", sensitivityAndSpecificity[0], sensitivityAndSpecificity[1]));
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		assertEquals(expectedResult, result);
		System.out.println("Test terminado ...");
		System.out.println("=============================================================================================================\n");
	}

	// @Test
	public void testDiscountFormula() {
		System.out.println("*************************************************************************************************************");

		boolean expectedResult = true;
		boolean result = true;

		try {
			System.out.println(1.0 + CostsUtils.calculateDiscount(1.0, 0.03, 0.0, 82.27 - 1.0));
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		assertEquals(expectedResult, result);
		System.out.println("Test terminado ...");
		System.out.println("=============================================================================================================\n");
	}

	/******************************************************************************************************************************************************/
	/****************************************************************** UTILS METHODS *********************************************************************/
	/******************************************************************************************************************************************************/
	private void shoDataPropertyAssertionFields(Map<String, String> instanceToClazz, DataPropertyAssertion assertion) {
		String literalType = "string";
		String[] tmp = (assertion.getLiteral().getDatatypeIRI() != null) ? assertion.getLiteral().getDatatypeIRI().split("#") : null;
		if (tmp != null) {
			literalType = (tmp.length > 1) ? tmp[1] : tmp[0];
		}

		System.out.println(String.format("%s[%s] ==> %s ==> %s::%s", 
				assertion.getNamedIndividual().getIRI(), 
				instanceToClazz.get(assertion.getNamedIndividual().getIRI()),
				assertion.getDataProperty().getIRI(), 
				assertion.getLiteral().getValue(), 
				literalType));
	}

	private void showObjectPropertyAssertionFields(Map<String, String> instanceToClazz, ObjectPropertyAssertion assertion) {
		System.out.println(String.format("%s[%s] ==> %s ==> %s[%s]", 
				((NamedIndividual) assertion.getRest().get(1).getValue()).getIRI(),
				instanceToClazz.get(((NamedIndividual) assertion.getRest().get(1).getValue()).getIRI()), 
				((ObjectProperty) assertion.getRest().get(0).getValue()).getIRI(),
				((NamedIndividual) assertion.getRest().get(2).getValue()).getIRI(), 
				instanceToClazz.get(((NamedIndividual) assertion.getRest().get(2).getValue()).getIRI())));
	}
}
