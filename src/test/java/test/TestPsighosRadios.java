/**
 * 
 */
package test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.w3c.xsd.owl2.Ontology;

import com.beust.jcommander.JCommander;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import es.ull.iis.ontology.radios.Constants;
import es.ull.iis.ontology.radios.json.schema4simulation.Schema4Simulation;
import es.ull.iis.ontology.radios.json.schema4simulation.builders.DiseaseBuilder;
import es.ull.iis.ontology.radios.json.schema4simulation.builders.OwlHelper;
import es.ull.iis.ontology.radios.utils.OntologyUtils;
import es.ull.iis.simulation.hta.DiseaseMain;
import es.ull.iis.simulation.hta.DiseaseMain.Arguments;
import es.ull.iis.simulation.hta.params.BasicConfigParams;
import es.ull.iis.simulation.hta.progression.Transition;
import es.ull.iis.simulation.hta.radios.RadiosExperimentResult;

public class TestPsighosRadios {
	public static void main(String[] args) {
		final Arguments arguments = new Arguments();
		try {
			JCommander jc = JCommander.newBuilder().addObject(arguments).build();
			Boolean useProgramaticArguments = true;
			if (useProgramaticArguments) {
				String params = "-n 1 -r 1 -dr 0 -q -pop 1 -dis 11";
				jc.parse(params.split(" "));
			} else {
				jc.parse(args);
			}
			BasicConfigParams.STUDY_YEAR = arguments.year;

			for (final Map.Entry<String, String> pInit : arguments.initProportions.entrySet()) {
				BasicConfigParams.INIT_PROP.put(pInit.getKey(), Double.parseDouble(pInit.getValue()));
			}

			String diseaseName = null;
			String radiosPath = null;
			int opcion = 2;
			if (opcion == 0) {
				diseaseName = "#PBD_ProfoundBiotinidaseDeficiency";
				radiosPath = "src/main/resources/owl/radios_PBD.owl";
			} else if (opcion == 1) {
				diseaseName = "#SCD_Disease_SickleCell";
				radiosPath = "src/main/resources/owl/radios_SCD.owl";
			} else if (opcion == 2) {
				diseaseName = "#RD1_Disease";
				radiosPath = "src/main/resources/owl/radios-test_disease1.owl";
			}
			
			Ontology radios = OntologyUtils.loadOntology(radiosPath);
			OwlHelper.initilize(radios);
			ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT).setSerializationInclusion(Include.NON_NULL).setSerializationInclusion(Include.NON_EMPTY);
			Schema4Simulation radiosDiseaseInstance = mapper.readValue(mapper.writeValueAsString(new Schema4Simulation(DiseaseBuilder.getDisease(diseaseName))).getBytes(), Schema4Simulation.class);			
			
			List<String> interventionsToCompare = new ArrayList<>();
			interventionsToCompare.add(Constants.CONSTANT_DO_NOTHING);
			interventionsToCompare.add("#PBD_InterventionScreening");
			RadiosExperimentResult result = DiseaseMain.runExperiment(arguments, radiosDiseaseInstance, interventionsToCompare, true, 0.8861);

			for (Transition transition : result.getTransitions()) {
				System.out.println(transition.getSrcManifestation().getName() + " --> " + transition.getDestManifestation().getName() + " ==> replacePrevious = " + transition.replacesPrevious());
			}
			System.out.println();
			System.out.println(result.getPrettySavedParams());
			System.out.println(new String(result.getSimResult().toByteArray()));
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

}
