package test;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;

import org.junit.Test;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import es.ull.iis.ontology.radios.Constants;
import es.ull.iis.ontology.radios.NodeData;
import es.ull.iis.ontology.radios.TreeNode;
import es.ull.iis.ontology.radios.json.databind.NodeStructureResource;
import es.ull.iis.ontology.radios.utils.TreantTreeUtils;
import es.ull.iis.ontology.radios.utils.TreeUtils;
import es.ull.iis.ontology.radios.wrappers.IntegerWrapper;

public class TestsTrees {
	@SuppressWarnings({ "rawtypes", "unchecked" })
	// @Test
	public void testPreOrden() {
		System.out.println("*************************************************************************************************************");

		boolean expectedResult = true;
		boolean result = true;

		try {
			TreeNode root = generateTree();

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			TreeUtils.preorden(root, Constants.CONSTANT_EMPTY_STRING, baos);
			System.out.println(new String(baos.toByteArray()));
		} catch (Exception e) {
			result = false;
		}

		assertEquals(expectedResult, result);
		System.out.println("=============================================================================================================\n");
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	// @Test
	public void testInOrden() {
		System.out.println("*************************************************************************************************************");

		boolean expectedResult = true;
		boolean result = true;

		try {
			TreeNode root = generateTree();

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			TreeUtils.inorden(root, Constants.CONSTANT_EMPTY_STRING, baos);
			System.out.println(new String(baos.toByteArray()));

		} catch (Exception e) {
			result = false;
		}

		assertEquals(expectedResult, result);
		System.out.println("=============================================================================================================\n");
	}

	@Test
	public void testInOrdenTreant() {
		System.out.println("*************************************************************************************************************");

		boolean expectedResult = true;
		boolean result = true;

		try {
			TreeNode<NodeData> root = generateTree();
			NodeStructureResource rootTreantNode = NodeStructureResource.toResource(root); 

			IntegerWrapper maxLevel = new IntegerWrapper();
			TreantTreeUtils.inorden(root, rootTreantNode, 0, maxLevel);
			
			TreantTreeUtils.completeChildsToMaxLevelTree(rootTreantNode, 0, maxLevel.value);
			
			ObjectMapper mapper = new ObjectMapper()
					.enable(SerializationFeature.INDENT_OUTPUT)
					.setSerializationInclusion(Include.NON_NULL)
					.setSerializationInclusion(Include.NON_EMPTY);
			System.out.println(mapper.writeValueAsString(rootTreantNode));
		} catch (Exception e) {
			result = false;
		}

		assertEquals(expectedResult, result);
		System.out.println("=============================================================================================================\n");
	}

	/******************************************************************************************************************************************************/
	/****************************************************************** UTILS METHODS *********************************************************************/
	/******************************************************************************************************************************************************/
	@SuppressWarnings({ "unused" })
	private TreeNode<NodeData> generateTree() {
		TreeNode<NodeData> root = new TreeNode<NodeData>(new NodeData("Root", "Root"));
		TreeNode<NodeData> a = root.addChild(new NodeData("A", "Leaf"));
		TreeNode<NodeData> b = root.addChild(new NodeData("B", "Middle"));
		TreeNode<NodeData> b1 = b.addChild(new NodeData("B1", "Leaf"));
//		TreeNode<NodeData> b2 = b.addChild(new NodeData("B2", "Leaf"));
//		TreeNode<NodeData> b3 = b.addChild(new NodeData("B3", "Middle"));
//		TreeNode<NodeData> b31 = b3.addChild(new NodeData("B31", "Middle"));
//		TreeNode<NodeData> b311 = b31.addChild(new NodeData("B311", "Middle"));
//		TreeNode<NodeData> b3111 = b311.addChild(new NodeData("B3111", "Leaf"));
//		TreeNode<NodeData> b3112 = b311.addChild(new NodeData("B3112", "Leaf"));
//		TreeNode<NodeData> b3113 = b311.addChild(new NodeData("B3113", "Leaf"));
//		TreeNode<NodeData> b3114 = b311.addChild(new NodeData("B3114", "Leaf"));
//		TreeNode<NodeData> b3115 = b311.addChild(new NodeData("B3115", "Leaf"));
//		TreeNode<NodeData> b3116 = b311.addChild(new NodeData("B3116", "Leaf"));
//		TreeNode<NodeData> b4 = b.addChild(new NodeData("B4", "Leaf"));
//		TreeNode<NodeData> b5 = b.addChild(new NodeData("B5", "Leaf"));
//		TreeNode<NodeData> c = root.addChild(new NodeData("C", "Leaf"));
//		TreeNode<NodeData> d = root.addChild(new NodeData("D", "Middle"));
//		TreeNode<NodeData> d1 = d.addChild(new NodeData("D1", "Leaf"));
//		TreeNode<NodeData> d2 = d.addChild(new NodeData("D2", "Leaf"));
		return root;
	}
}
