package test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import es.ull.iis.ontology.radios.Constants;
import es.ull.iis.ontology.radios.NodeData;
import es.ull.iis.ontology.radios.PropertyData;
import es.ull.iis.ontology.radios.TreeNode;
import es.ull.iis.ontology.radios.exceptions.TranspilerException;
import es.ull.iis.ontology.radios.utils.TreeUtils;

public class TestCalculateAlternatives {
	public static void metodo(TreeNode<NodeData> node, List<String> tmp) {
		if (!tmp.isEmpty()) {
			String x = tmp.get(0);
			TreeNode<NodeData> n = node.addChild(new NodeData(x, null));
			// System.out.println(tmp);
			metodo(n, new ArrayList<String>(tmp.subList(tmp.size() - (tmp.size() - 1), tmp.size())));
			TreeNode<NodeData> m = node.addChild(new NodeData(x + "'", null));
			metodo(m, new ArrayList<String>(tmp.subList(tmp.size() - (tmp.size() - 1), tmp.size())));
		}
	}

	@SuppressWarnings("unused")
	private static Map<String, PropertyData> createProperties(Double onSetAge, Double endAge, Double utility, int option) {
		Map<String, PropertyData> properties = new HashMap<>();

		if (option == 0) {
		} else if (option == 1) {
			if (endAge == Double.MAX_VALUE) { Double lifeExpectancy = endAge = 82.27;}
			properties.put(Constants.DATAPROPERTY_END_AGE, new PropertyData(Constants.DATAPROPERTY_END_AGE, String.valueOf(endAge), Constants.CONSTANT_DOUBLE_TYPE));
		} else if (option == 2) {
			properties.put(Constants.DATAPROPERTY_ONSET_AGE, new PropertyData(Constants.DATAPROPERTY_ONSET_AGE, String.valueOf(onSetAge), Constants.CONSTANT_DOUBLE_TYPE));
		} else if (option == 3) {
			properties.put(Constants.DATAPROPERTY_ONSET_AGE, new PropertyData(Constants.DATAPROPERTY_ONSET_AGE, String.valueOf(onSetAge), Constants.CONSTANT_DOUBLE_TYPE));
			if (endAge == Double.MAX_VALUE) { Double lifeExpectancy = endAge = 82.27;}
			properties.put(Constants.DATAPROPERTY_END_AGE, new PropertyData(Constants.DATAPROPERTY_END_AGE, String.valueOf(endAge), Constants.CONSTANT_DOUBLE_TYPE));
		}

		properties.put(Constants.CUSTOM_PROPERTY_UTILITY_VALUE, new PropertyData(Constants.DATAPROPERTY_ONSET_AGE, String.valueOf(utility), Constants.CONSTANT_DOUBLE_TYPE));
		return properties;
	}

	private static Map<String, Map<String, PropertyData>> loadManifestations() {
		Map<String, Map<String, PropertyData>> result = new HashMap<>();

		result.put("man6", createProperties(18.0, 30.0, 0.886, 3));
		result.put("man2", createProperties(0.0, Double.MAX_VALUE, 0.886, 0));
		result.put("man5", createProperties(12.0, Double.MAX_VALUE, 0.886, 2));
		result.put("man1", createProperties(0.0, 10.0, 0.816, 1));
		result.put("man3", createProperties(2.0, Double.MAX_VALUE, 0.886, 2));
		result.put("man4", createProperties(3.0, 18.0, 0.886, 3));
		return result;
	}
	
	private static List<String> transformManifestationsToListForOrder(Map<String, Map<String, PropertyData>> manifestations, Double lifeExpectancy) {		
		List<String> result = new ArrayList<String>();
		Double value = null;
		for (String manifestation : manifestations.keySet()) {
			Map<String, PropertyData> properties = manifestations.get(manifestation);
			StringBuilder sb = new StringBuilder();
			
			value = 0.0;
			if (properties.containsKey(Constants.DATAPROPERTY_ONSET_AGE)) {
				value = Double.valueOf(properties.get(Constants.DATAPROPERTY_ONSET_AGE).getValue());
			}
			sb.append(String.format(Locale.US, "%07.3f", value).replace(".", "")).append("_");

			value = lifeExpectancy;
			if (properties.containsKey(Constants.DATAPROPERTY_END_AGE)) {
				value = Double.valueOf(properties.get(Constants.DATAPROPERTY_END_AGE).getValue());
			}
			sb.append(String.format(Locale.US, "%07.3f", value).replace(".", "")).append(":::");
			
			sb.append(manifestation);
			result.add(sb.toString());
		}
		return result;
	}

	public static <T extends Comparable<? super T>> List<T> asSortedList(Collection<T> c) {
	  List<T> list = new ArrayList<T>(c);
	  java.util.Collections.sort(list);
	  return list;
	}	

	public static Map<String, Map<String, PropertyData>> transformHashMapToLinkedHashMap (Map<String, Map<String, PropertyData>> manifestations, List<String> sortedKeyList, String splitPattern, Integer splittedIndex) {
		Map<String, Map<String, PropertyData>> result = new LinkedHashMap<String, Map<String,PropertyData>>();
		for (String item : sortedKeyList) {
			String key = item; 
			if (splitPattern != null) {
				key = item.split(splitPattern)[splittedIndex];
			}			
			result.put(key, manifestations.get(key));
		}
		return result;
	}
	
	public static void main(String[] args) throws TranspilerException, IOException {
		int opcion = 1;

		if (opcion == 0) {
			List<String> tmp = new ArrayList<String>();
			tmp.add("A");
			tmp.add("B");
			tmp.add("C");

			System.out.println("***");
			TreeNode<NodeData> root = new TreeNode<NodeData>(new NodeData("root", null));
			metodo(root, tmp);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			TreeUtils.inorden(root, "", baos);
			System.out.println(new String(baos.toByteArray()));
		} else if (opcion == 1) {
			Map<String, Map<String, PropertyData>> manifestations = loadManifestations();
			
			List<String> sorted = asSortedList(transformManifestationsToListForOrder(manifestations, 82.27));
			System.out.println(sorted);			
			Map<String, Map<String, PropertyData>> result = transformHashMapToLinkedHashMap (manifestations, sorted, ":::", 1);			
			for (String manifestation : result.keySet()) {
				Map<String, PropertyData> properties = manifestations.get(manifestation);
				for (String property : properties.keySet()) {
					System.out.println(String.format("Manifestation [%s], Property-Key [%s], Property-Value [%s]", manifestation, property, properties.get(property).getValue()));
				}
				
			}
		}
	}

}
