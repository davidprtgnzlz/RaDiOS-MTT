package test;

import java.util.HashMap;
import java.util.Map;

public class TestMaps {
	public static Map<String, Map<String, String>> cloneHashMap (Map<String, Map<String, String>> original) {
		Map<String, Map<String, String>> copy = new HashMap<String, Map<String, String>>();

		for (String key: original.keySet()) {
			copy.put(new String(key), new HashMap<String, String> (original.get(key)));
		}

		return copy;
	}	
	
	public static void main(String[] args) {
		Map<String, String> c = new HashMap<String, String>();
		c.put("cv", "321");
		c.put("modelo", "m3");
		
		Map<String, Map<String, String>> a = new HashMap<String, Map<String, String>>();
		a.put("bmw", c);		
		System.out.println(a);

		Map<String, Map<String, String>> b = TestMaps.cloneHashMap(a); 
		System.out.println(b);
		
		b.get("bmw").put("cv", "343");
		System.out.println(a);
		System.out.println(b);

		a.get("bmw").put("modelo", "m3e36");
		System.out.println(a);
		System.out.println(b);
		
		System.out.println("\n");
	}
}
