package test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import es.ull.iis.ontology.radios.Constants;

class RegExp {
	private String name;
	private String value;

	public RegExp(String name, String value) {
		this.name = name;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return String.format("%s {%s}", name, value);
	}	
}

public class TestRegExp {
	public static void main(String[] args) {
		List<String> expressions = new ArrayList<>();
		expressions.add("-9.564#BETA(65,50)::#string");
		expressions.add("0.564#BETA(65,50)::#string");
		expressions.add("1.0E-5#BETA(8, 540955)::#string");
		expressions.add("1.0E-5#BETA(8, 540955)");
		expressions.add("0.816#BETA(1.71,0.39)");		
		expressions.add("-9.564#BETA(65,50)");
		expressions.add("0.0::#double");
		expressions.add("0.0");
		expressions.add("*0.0::#string");
		expressions.add("*0.0");

		List<RegExp> regexps = new ArrayList<>();
		regexps.add(new RegExp("REGEX_ANYEXPRESION_TYPE", Constants.REGEX_ANYEXPRESION_TYPE));
		regexps.add(new RegExp("REGEX_NUMERICVALUE_DISTRO", Constants.REGEX_NUMERICVALUE_DISTRO));
		regexps.add(new RegExp("REGEX_OPERATION_NUMERICVALUE", Constants.REGEX_OPERATION_NUMERICVALUE));
		regexps.add(new RegExp("REGEX_NUMERICVALUE", Constants.REGEX_NUMERICVALUE));

		for (RegExp regexp : regexps) {
			StringBuilder sb = new StringBuilder("Regular Expresion [" + regexp + "]:\n");
			for (String expression : expressions) {
				sb.append(String.format("\t%s => %s\n", expression, expression.replaceAll("\\s+", "").matches(regexp.getValue())));
			}

			System.out.println(sb.toString());
		}

		System.out.println(Arrays.asList("1.0E-5#BETA(8, 540955)".split(Constants.CONSTANT_SPLIT_TYPE)));
		System.out.println("*0.0".charAt(0));
		System.out.println("*0.0".substring(1));
		System.out.println(String.format(Locale.US, "%020.7f", (new Double("2.3565874521"))));
	}

}
