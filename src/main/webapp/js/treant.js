
var tree_structure = {
    chart: {
        container: "#DecisionTree",
        levelSeparation: 5,
        siblingSeparation: 20,
        subTeeSeparation: 20,
        rootOrientation: "WEST",

        node: {
            HTMLclass: "decisiontree-draw",
            drawLineThrough: true
        },

        connectors: {
            type: "straight",
            style: {
                "stroke-width": 2,
                "stroke": "#777"
            }
        }
    },

    nodeStructure: {
        text: {
            name: "PBD",
            desc: "Profound DB",
        },
        connectors: {
            style: {
                'arrow-end': 'square-wide-long'
            }
        },
        children: [
            {
                text: {
                    name: "Screening",
                    desc: "p=0.967",
                },
                connectors: {
                    style: {
                        'arrow-start': 'oval-wide-long'
                    }
                },
                children: [
                    {
                        text: {
                            name: "Disease",
                            desc: "p=0.365"
                        },
                        connectors: {
                            style: {
                                'arrow-start': 'oval-wide-long'
                            }
                        },
                        children: [
                            {
                                text: {
                                    name: "False negative\n\b",
                                    desc: ""
                                },
                                connectors: {
                                    style: {
                                        'arrow-start': 'oval-wide-long'
                                    }
                                },
                                children: [
                                    {
                                        text: {
                                            name: "Sub Decision Tree\n\b",
                                            desc: ""
                                        },
                                        connectors: {
                                            style: {
                                                'arrow-start': 'invblock-wide-long'
                                            }
                                        },
                                    }
                                ]
                            },
                            {
                                text: {
                                    name: "True positive",
                                    desc: "p=0.456"
                                },
                                children: [
                                    {
                                        text: {
                                            name: "\b",
                                            desc: ""
                                        },
                                        connectors: {
                                            style: {
                                                'arrow-start': 'invblock-wide-long'
                                            }
                                        },
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        text: {
                            name: "No disease",
                            desc: "p=0.345"
                        },
                        connectors: {
                            style: {
                                'arrow-start': 'oval-wide-long'
                            }
                        },
                        children: [
                            {
                                text: {
                                    name: "False positive",
                                    desc: "p=0.345"
                                },
                                children: [
                                    {
                                        text: {
                                            name: "\b",
                                            desc: ""
                                        },
                                        connectors: {
                                            style: {
                                                'arrow-start': 'invblock-wide-long'
                                            }
                                        },
                                    }
                                ]
                            },
                            {
                                text: {
                                    name: "True negative",
                                    desc: "p=0.342"
                                },
                                children: [
                                    {
                                        text: {
                                            name: "\b",
                                            desc: ""
                                        },
                                        connectors: {
                                            style: {
                                                'arrow-start': 'invblock-wide-long'
                                            }
                                        },
                                    }
                                ]
                            }
                        ]
                    }
                ]
            },
            {
                text: {
                    name: "No Screening",
                    desc: "p=0.342"
                },
                connectors: {
                    style: {
                        'arrow-start': 'oval-wide-long'
                    }
                },
                children: [
                    {
                        text: {
                            name: "Disease",
                            desc: "p=0.342"
                        },
                        connectors: {
                            style: {
                                'arrow-start': 'oval-wide-long'
                            }
                        },
                        children: [
                            {
                                text: {
                                    name: "Sub Decision Tree\n\b",
                                    desc: ""
                                },
                                children: [
                                    {
                                        text: {
                                            name: "\b",
                                            desc: ""
                                        },
                                        connectors: {
                                            style: {
                                                'arrow-start': 'invblock-wide-long'
                                            }
                                        },
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        text: {
                            name: "No disease",
                            desc: "p=0.749"
                        },
                        children: [
                            {
                                text: {
                                    name: "\b",
                                    desc: ""
                                },
                                children: [
                                    {
                                        text: {
                                            name: "\b",
                                            desc: ""
                                        },
                                        connectors: {
                                            style: {
                                                'arrow-start': 'invblock-wide-long'
                                            }
                                        },
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
};
