const app = new Vue({
   el: '#app',

   data: {
      mostrar: false,
      mensaje: "Hola Vue!",
      imagen: "http://laravelacademy.org/wp-content/uploads/2016/08/00-featured-vuejs-logo-simple-256x128.jpg",
      coches: [
         { name: "BMW", url: 'https://www.google.es' },
         { name: "Porsche", url: 'https://www.google.es' },
         { name: "Ferrari", url: 'https://www.google.es' }
      ]
   },

   methods: {
      toggleMostrar: function () {
         this.mostrar = !this.mostrar;
      }
   }
})