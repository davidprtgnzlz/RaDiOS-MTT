const app = new Vue({
   el: '#app',

   data: {
      showDecisionTree: false,
      showDesSimulation: false,
      showInputParameters: false,
      showLoading: false,
      
      myTreeStructure: {},
      diseaseDatasheet: "",
      
      selectedOwlFile: "",
      selectedDisease: "",
      selectedInterventionA: "",
      selectedInterventionB: "",
      selectedClinicalDiagnosisStrategy: "",
      
      selectedDiscountRateCosts: "0.0",
      selectedDiscountRateEffects: "0.0",
      selectedUtilityGeneralPopulation: "0.8861",
      selectedTimeHorizon: "-1", 
      selectedNumSimulations: "0", 
      selectedNumPatients: "1000", 
      allPatientsAffected: false, 

      messageEvent: "",
      levelEvent: "",
      
      owlFiles: [
    	  {"id": "100", "description": "Real rare diseases - PBD"},
    	  {"id": "101", "description": "Real rare diseases - SCD"},
    	  {"id": "1", "description": "Test rare disease 1"},
    	  {"id": "2", "description": "Test rare disease 2"},
    	  {"id": "3", "description": "Test rare disease 3"},
    	  {"id": "4", "description": "Test rare disease 4"}
      ],
      diseases: [],
      interventions: [],
      strategies: []
   },

   methods: {
	  resetSelections: function () {
	      this.selectedDisease = this.selectedInterventionA = this.selectedInterventionB = this.selectedClinicalDiagnosisStrategy = "";
	      this.showDecisionTree = this.showDesSimulation = this.showInputParameters = this.showLoading = false;
	      this.diseases = this.interventions = this.strategies = [];
	      this.selectedDiscountRateCosts = "0.0";
	      this.selectedDiscountRateEffects = "0.0";
	      this.selectedUtilityGeneralPopulation = "0.8861";
	      this.selectedTimeHorizon = "-1";
	      this.selectedNumSimulations = "0"; 
	      this.selectedNumPatients = "1000";
	      this.allPatientsAffected = false;
	  },
	  
	  addEventHoverToNode: function () {
		  $(".node").hover(
				  function () {
					  var titleHtml = $(this).children(".node-title").html(); 
					  if (titleHtml !== undefined && titleHtml != "") {
						  var title = "<pre>" + titleHtml + "</pre>";
						  $('<div class="node-tip"></div>').html(title).appendTo('body').fadeIn('slow');
					  } 
				  }, 
				  function () {
					  $('.node-tip').remove();
				  }
		  ).mousemove(
				  function (e) {
					  var mousex = e.pageX + 20;
					  var mousey = e.pageY + 10;
					  $('.node-tip').css({ top: mousey, left: mousex });
				  }
		  );
	  },
	  
	  showPopUp: function (message, level) {
		  this.messageEvent = message;
		  this.levelEvent = level;
		  document.getElementById('launchPopUp').click();
	  },
	   
	  generateDecisionTree: function () {
    	  this.showDesSimulation = false;
		  this.showDecisionTree = true;
		  this.showLoading = true;
          axios.post(
        		  'api/v2/decision-tree/asJson',
        		  {
        			  "disease" : this.selectedDisease,
        			  "interventions" : [ this.selectedInterventionA , this.selectedInterventionB ],
        			  "clinicalDiagnosisStrategy" : this.selectedClinicalDiagnosisStrategy,
        			  "discountRateCosts" : this.selectedDiscountRateCosts,
        			  "discountRateEffects" : this.selectedDiscountRateEffects,
        			  "utilityGeneralPopulation" : this.selectedUtilityGeneralPopulation,
        			  "framework" : "treant"
        		  }
          ).then(response => {              
              if (response.data.error === undefined) {
                  this.myTreeStructure = response.data;
                  new Treant( this.myTreeStructure );
                  this.getDiseaseDatasheet();
                  this.showPopUp("Generated Decision Tree ...", "INFO");
              } else {
            	  this.showPopUp(response.data.error, "ERROR");
              }
              this.showLoading = false;
              this.addEventHoverToNode();
          }).catch(error => {
        	  console.log(error);
        	  this.showLoading = false;
        	  this.showPopUp(error, "ERROR");
          });		
      },
      
      createCytoscapeStructure: function (elementsData) {
          var cy = cytoscape({
              container: document.getElementById('cy'),              
              layout: {
                name: 'concentric',
                concentric: function(n){ return n.id() === 'j' ? 100 : 0; },
                levelWidth: function(nodes){ return 10; },
                minNodeSpacing: 200
              },
              style: [
                {
                  selector: 'node[name]',
                  style: {
                    'content': 'data(name)',
                    'background-color': function(e) {
						if (e.data('type') === 'CHRONIC') {
							if (e.data('replacePrevious') === true) {
								return '#0222B4';
							} else {
								return '#FF0000';
							}
						} else if (e.data('type') === 'ACUTE') {
							return '#FFAF54';
						} else if (e.data('name') === 'HEALTHY') { 
							return '#42CD6A';
						}
                    },
                    shape: function(e) {
						if (e.data('type') === 'CHRONIC') {
							return 'round-octagon';
						} else if (e.data('type') === 'ACUTE') {
							return 'round-hexagon'; 
						} else if (e.data('name') === 'HEALTHY') { 
							return 'circle';
						}
                    }
                  }
                },
                {
                  selector: 'edge',
                  style: {
                    'curve-style': 'bezier',
                    'target-arrow-shape': 'triangle'
                  }
                }
              ],
              elements: elementsData
          });          
    	  return cy;
      },
      
      desSimulation: function () {
		  this.showDecisionTree = false;
    	  this.showDesSimulation = true;
		  this.showLoading = true;
          axios.post(
        		  'api/v2/des-simulation',
        		  {
        			  "disease" : this.selectedDisease,
        			  "interventions" : [ this.selectedInterventionA , this.selectedInterventionB ],
        			  "clinicalDiagnosisStrategy" : this.selectedClinicalDiagnosisStrategy,
        			  "discountRateCosts" : this.selectedDiscountRateCosts,
        			  "discountRateEffects" : this.selectedDiscountRateEffects,
        			  "utilityGeneralPopulation" : this.selectedUtilityGeneralPopulation,
        			  "timeHorizon" : this.selectedTimeHorizon,
        			  "numSimulations" : this.selectedNumSimulations,
        			  "numPatients" : this.selectedNumPatients,
        			  "allPatientsAffected" : this.allPatientsAffected
        		  }
          ).then(response => {              
              if (response.data.error === undefined) {
            	  console.log(response.data);
                  this.showPopUp("The discrete event simulation model has been successfully executed ...", "INFO");

                  // Create manifestations graph
                  this.createCytoscapeStructure(response.data.cyJson);

                  // Create incidences linechart
                  if (response.data.linecharts !== undefined && response.data.linecharts.linechart !== undefined) {
                	  var config = {responsive: true};
                	  for (var i = 0; i < response.data.linecharts.linechart.length; i++) { 
                    	  Plotly.newPlot("incidenceLineChart"+i, response.data.linecharts.linechart[i].traces, response.data.linecharts.linechart[i].layout, config);
                	  }
                  }
                  
                  // Create datasheets
                  $('#secondOrderParameters').html(buildHtmlTable(response.data.parameters, "secondTable", "Second order parameters"));
                  $('#deterministicSimulationResult').html(buildHtmlTableFromLists(response.data.simulationResult.deterministic, "thirdTable", "Deterministic PSIGHOS Simulation Result"));
                  if (Object.keys(response.data.simulationResult.summaryProbabilistic).length !== 0) {
                      $('#summaryProbabilisticSimulationResult').html(buildHtmlTableFromLists(response.data.simulationResult.summaryProbabilistic, "thirdTable", "Summary Probabilistic PSIGHOS Simulation Result"));
                  }
                  if (Object.keys(response.data.simulationResult.probabilistic).length !== 0) {
                      $('#probabilisticSimulationResult').html(buildHtmlTableFromLists(response.data.simulationResult.probabilistic, "thirdTable", "Probabilistic PSIGHOS Simulation Result"));
                  }
              } else {
            	  this.showPopUp(response.data.error, "ERROR");
              }
              this.showLoading = false;
          }).catch(error => {
        	  console.log(error);
        	  this.showLoading = false;
        	  this.showPopUp(error, "ERROR");
          });
      },
      
      loadDiseases: function() {
    	  this.showLoading = true;
          axios.get('api/v2/' + this.selectedOwlFile + '/diseases')
          .then(response => {
             this.diseases = response.data;
             this.showInputParameters = true;
             this.showLoading = false;
          }).catch(error => {
             console.log(error);
             this.diseases = [];
             this.showLoading = false;
             this.showPopUp(error, "ERROR");
          });
 	  },
 	  
      getDiseaseDatasheet: function() {
          this.showLoading = true;
          axios.get('api/v2/' + this.selectedDisease + '/datasheet')
          .then(response => {
             this.diseaseDatasheet = response.data;
             this.showLoading = false;
          }).catch(error => {
             console.log(error);
             this.showLoading = false;
             this.diseaseDatasheet = 'Data could not be recovered !!';
             this.showPopUp(error, "ERROR");
          });
 	  },
 	  
      loadInterventions: function() {
          this.showLoading = true;
          axios.get('api/v2/' + this.selectedDisease + '/interventions')
          .then(response => {
             this.interventions = response.data;
             this.showLoading = false;
          }).catch(error => {
             console.log(error);
             this.interventions = [];
             this.showLoading = false;
             this.showPopUp(error, "ERROR");
          });
 	  },
 	  
      loadStrategies: function() {
          this.showLoading = true;
          axios.get('api/v2/' + this.selectedDisease + '/clinicalDiagnosisStrategy')
          .then(response => {
             this.strategies = response.data;
             this.showLoading = false;
          }).catch(error => {
             console.log(error);
             this.strategies = [];
             this.showLoading = false;
             this.showPopUp(error, "ERROR");
          });
 	  }
   }
})