var _table_ = document.createElement('table'), 
	_thead_ = document.createElement('thead'), 
	_tbody_ = document.createElement('tbody'), 
	_tr_ = document.createElement('tr'), 
	_th_ = document.createElement('th'), 
	_td_ = document.createElement('td');

// Builds the HTML Table out of myList json data from Ivy restful service.
function buildHtmlTable(arr, htmlClass, title) {
	var table = _table_.cloneNode(false), 
		columns = addAllColumnHeaders(arr, table, title);
	for (var i = 0, maxi = arr.length; i < maxi; ++i) {
		var tr = _tr_.cloneNode(false);
		for (var j = 0, maxj = columns.length; j < maxj; ++j) {
			var td = _td_.cloneNode(false);
			cellValue = arr[i][columns[j]];
			td.appendChild(document.createTextNode(cellValue || ''));
			tr.appendChild(td);
		}
		table.appendChild(tr);
	}
	table.className = htmlClass;
	return table;
}

// Adds a header row to the table and returns the set of columns. Need to do union of keys from all records as some records may not contain all records.
function addAllColumnHeaders(arr, table, title) {
	var columnSet = [], 
		tr = _tr_.cloneNode(false);
	for (var i = 0, l = arr.length; i < l; i++) {
		for (var key in arr[i]) {
			if (arr[i].hasOwnProperty(key) && columnSet.indexOf(key) === -1) {
				columnSet.push(key);
				var th = _th_.cloneNode(false);
				th.appendChild(document.createTextNode(key.toUpperCase()));
				tr.appendChild(th);
			}
		}
	}
	
	var trTitle = _tr_.cloneNode(false);
	var thTitle = _th_.cloneNode(false);
	thTitle.colSpan = columnSet.length;
	thTitle.appendChild(document.createTextNode(title.toUpperCase()));
	trTitle.appendChild(thTitle);
	table.appendChild(trTitle);	
	
	table.appendChild(tr);
	return columnSet;
}

function buildHtmlTableFromLists(arr, htmlClass, title) {
	var table = _table_.cloneNode(false), 
		columns = addAllColumnHeadersFromList(arr.headers, table, title);
	var tBody = _tbody_.cloneNode(false);
	for (var i = 0, maxi = arr.values.length; i < maxi; i++) {
		var tr = _tr_.cloneNode(false);
		for (var j = 0, maxj = arr.values[i].length; j < maxj; j++) {
			var td = _td_.cloneNode(false);
			cellValue = arr.values[i][j];
			td.appendChild(document.createTextNode(cellValue || ''));
			tr.appendChild(td);
		}
		tBody.appendChild(tr);
	}
	table.appendChild(tBody);
	table.className = htmlClass;
	return table;
}

function addAllColumnHeadersFromList(arr, table, title) {
	var columnSet = [], 
		tr = _tr_.cloneNode(false);
	if (arr !== undefined) {
		for (var i = 0, l = arr.length; i < l; i++) {
			columnSet.push(arr[i]);
			var th = _th_.cloneNode(false);
			if (arr[i].length > 10) {			
				th.appendChild(document.createTextNode(arr[i].substring(0, 10).concat("...").toUpperCase()));
				th.title = arr[i].toUpperCase();
			} else {
				th.appendChild(document.createTextNode(arr[i].toUpperCase()));
			}
			tr.appendChild(th);
		}
	}
	
	var trTitle = _tr_.cloneNode(false);
	var thTitle = _th_.cloneNode(false);
	thTitle.colSpan = columnSet.length;
	thTitle.appendChild(document.createTextNode(title.toUpperCase()));
	trTitle.appendChild(thTitle);
	trTitle.className = 'tableTitle';

	var tHead = _thead_.cloneNode(false);
	tHead.appendChild(trTitle);
	tHead.appendChild(tr);
	table.appendChild(tHead);
	return columnSet;
}