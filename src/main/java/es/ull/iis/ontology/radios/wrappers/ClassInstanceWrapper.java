package es.ull.iis.ontology.radios.wrappers;

import java.util.List;

public class ClassInstanceWrapper {
	private String name;
	private List<DataPropertyWrapper> properties;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<DataPropertyWrapper> getProperties() {
		return properties;
	}

	public void setProperties(List<DataPropertyWrapper> properties) {
		this.properties = properties;
	}
}
