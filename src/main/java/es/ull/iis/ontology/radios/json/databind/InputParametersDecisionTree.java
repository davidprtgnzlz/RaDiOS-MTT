package es.ull.iis.ontology.radios.json.databind;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class InputParametersDecisionTree {
	private String disease;
	private List<String> interventions;
	private String clinicalDiagnosisStrategy;
	private Double discountRateCosts;
	private Double discountRateEffects;
	private Double utilityGeneralPopulation;
	private String framework;

	public String getDisease() {
		return disease;
	}

	public void setDisease(String disease) {
		this.disease = disease;
	}

	public List<String> getInterventions() {
		return interventions;
	}

	public void setInterventions(List<String> interventions) {
		this.interventions = interventions;
	}

	public String getClinicalDiagnosisStrategy() {
		return clinicalDiagnosisStrategy;
	}

	public void setClinicalDiagnosisStrategy(String clinicalDiagnosisStrategy) {
		this.clinicalDiagnosisStrategy = clinicalDiagnosisStrategy;
	}

	public String getFramework() {
		return framework;
	}

	public void setFramework(String framework) {
		this.framework = framework;
	}

	public Double getDiscountRateCosts() {
		return discountRateCosts;
	}

	public void setDiscountRateCosts(Double discountRateCosts) {
		this.discountRateCosts = discountRateCosts;
	}

	public Double getDiscountRateEffects() {
		return discountRateEffects;
	}

	public void setDiscountRateEffects(Double discountRateEffects) {
		this.discountRateEffects = discountRateEffects;
	}

	public Double getUtilityGeneralPopulation() {
		return utilityGeneralPopulation;
	}

	public void setUtilityGeneralPopulation(Double utilityGeneralPopulation) {
		this.utilityGeneralPopulation = utilityGeneralPopulation;
	}

	public static void main(String[] args) throws JsonProcessingException {
		InputParametersDecisionTree inputParametersDecisionTree = new InputParametersDecisionTree();
		inputParametersDecisionTree.setClinicalDiagnosisStrategy("PBD_ClinicalDiagnosisStrategy");
		inputParametersDecisionTree.setDiscountRateCosts(0.03);
		inputParametersDecisionTree.setDiscountRateEffects(0.03);
		inputParametersDecisionTree.setUtilityGeneralPopulation(1.0);
		inputParametersDecisionTree.setDisease("PBD_ProfoundBiotinidaseDeficiency");
		inputParametersDecisionTree.setFramework("treant");
		List<String> interventions = new ArrayList<>();
		interventions.add("PBD_InterventionNoScreening");
		interventions.add("PBD_InterventionScreening");
		inputParametersDecisionTree.setInterventions(interventions);

		ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT).setSerializationInclusion(Include.NON_NULL).setSerializationInclusion(Include.NON_EMPTY);
		System.out.println(mapper.writeValueAsString(inputParametersDecisionTree));
	}

}
