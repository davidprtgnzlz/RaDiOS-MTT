package es.ull.iis.ontology.radios.json.schema4simulation.builders;

import java.util.ArrayList;
import java.util.List;

import es.ull.iis.ontology.radios.Constants;
import es.ull.iis.ontology.radios.json.schema4simulation.Drug;
import es.ull.iis.ontology.radios.json.schema4simulation.Treatment;
import es.ull.iis.ontology.radios.json.schema4simulation.TreatmentStrategy;

public class TreatmentBuilder {
	public static List<TreatmentStrategy> getTreatmentStrategies(String objectName) {
		List<TreatmentStrategy> result = new ArrayList<>();
		List<String> childs = OwlHelper.getChildsByClassName(objectName, Constants.CLASS_TREATMENTSTRATEGY);
		for (String childName: childs) {
			TreatmentStrategy treatmentStrategy = new TreatmentStrategy();
			treatmentStrategy.setName(childName);
			treatmentStrategy.setCosts(CrossBuilder.getCosts(childName));
			treatmentStrategy.setGuidelines(CrossBuilder.getGuidelines(childName));
			treatmentStrategy.setPercentageTreated(OwlHelper.getDataPropertyValue(childName, Constants.DATAPROPERTY_PERCENTAGETREATED));
			treatmentStrategy.setTreatments(getTreatments(childName));					
			result.add(treatmentStrategy);
		}
		return !result.isEmpty() ? result : null;
	}
	
	public static List<Treatment> getTreatments(String treatmentStrategyName) {
		List<Treatment> result = new ArrayList<>();
		List<String> treatmentStepsStrategy = OwlHelper.getChildsByClassName(treatmentStrategyName, Constants.CLASS_TREATMENTSTEPSTRATEGY);
		for (String treatmentStepStrategyName: treatmentStepsStrategy) {
			List<String> treatments = OwlHelper.getChildsByClassName(treatmentStepStrategyName, Constants.CLASS_TREATMENT);
			for (String treatmentName: treatments) {
				result.add(getTreatment(treatmentName));
			}
		}
		return !result.isEmpty() ? result : null;
	}
	
	public static Treatment getTreatment(String treatmentName) {
		Treatment treatment = new Treatment();
		treatment.setName(treatmentName);
		treatment.setCosts(CrossBuilder.getCosts(treatmentName));
		treatment.setDrugs(getDrugs(treatmentName));
		treatment.setGuidelines(CrossBuilder.getGuidelines(treatmentName));
		treatment.setPercentageTreated(OwlHelper.getDataPropertyValue(treatmentName, Constants.DATAPROPERTY_PERCENTAGETREATED));
		treatment.setPercentageNext(OwlHelper.getDataPropertyValue(treatmentName, Constants.DATAPROPERTY_PERCENTFORTHENEXT));
		return treatment;
	}
	
	public static List<Drug> getDrugs(String objectName) {
		List<Drug> result = new ArrayList<>();
		List<String> drugs = OwlHelper.getChildsByClassName(objectName, Constants.CLASS_DRUG);
		for (String drugName: drugs) {
			result.add(getDrug(drugName));
		}
		return !result.isEmpty() ? result : null;
	}

	public static Drug getDrug(String drugName) {
		Drug drug = new Drug();
		drug.setName(drugName);
		drug.setCosts(CrossBuilder.getCosts(drugName));
		drug.setGuidelines(CrossBuilder.getGuidelines(drugName));
		return drug;
	}
}
