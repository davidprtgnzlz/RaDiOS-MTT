package es.ull.iis.ontology.radios.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.w3c.xsd.owl2.Ontology;

import es.ull.iis.ontology.radios.Constants;
import es.ull.iis.ontology.radios.NodeData;
import es.ull.iis.ontology.radios.PropertyData;
import es.ull.iis.ontology.radios.TreeNode;
import es.ull.iis.ontology.radios.exceptions.TranspilerException;
import es.ull.iis.ontology.radios.json.databind.InputParametersDecisionTree;
import es.ull.iis.ontology.radios.processors.DecisionTreeNodeProcessor;
import es.ull.iis.ontology.radios.utils.CostsUtils;
import es.ull.iis.ontology.radios.utils.OntologyUtils;

public interface DecisionTreeBuilderService {
	@SuppressWarnings("unused")
	public static TreeNode<NodeData> buildDecisionTreeFromOntology(Ontology ontology, InputParametersDecisionTree inputParametersDecisionTree) throws TranspilerException {
		Map<String, String> instancesToClazz = new HashMap<String, String>();
		DataStoreService.eTLClassIndividuals(ontology, new HashMap<String, List<String>>(), instancesToClazz, "#Disease");
		
		String diseaseName = (inputParametersDecisionTree.getDisease().startsWith("#")) ? inputParametersDecisionTree.getDisease() : "#" + inputParametersDecisionTree.getDisease();
		String clinicalDiagnosisStrategy = inputParametersDecisionTree.getClinicalDiagnosisStrategy();
		if (!clinicalDiagnosisStrategy.startsWith("#")) {
			clinicalDiagnosisStrategy = "#" + clinicalDiagnosisStrategy;
		}
		DataStoreService.initializeDiseaseDatasheet(diseaseName);
		
		List<String> interventionsToCompare = new ArrayList<>();
		for (String intervention : inputParametersDecisionTree.getInterventions()) {
			interventionsToCompare.add((intervention.startsWith("#")) ? intervention : "#" + intervention);
		}
		
		TreeNode<NodeData> root = null;
		if (instancesToClazz.get(diseaseName) == null) {
			return null;
		} else {
			root = new TreeNode<NodeData>(new NodeData(diseaseName, instancesToClazz.get(diseaseName)));
		}

		Double discountRateCosts = (inputParametersDecisionTree.getDiscountRateCosts() != null) ? inputParametersDecisionTree.getDiscountRateCosts() : 0.0;
		DataStoreService.storePropertyDataIntoDiseaseDatacheet(diseaseName, new PropertyData(Constants.DATASHEET_DISCOUNT_RATE_COSTS, String.valueOf(discountRateCosts), Constants.CONSTANT_DOUBLE_TYPE));
		Double discountRateEffects = (inputParametersDecisionTree.getDiscountRateEffects() != null) ? inputParametersDecisionTree.getDiscountRateEffects() : 0.0;
		DataStoreService.storePropertyDataIntoDiseaseDatacheet(diseaseName, new PropertyData(Constants.DATASHEET_DISCOUNT_RATE_EFFECTS, String.valueOf(discountRateEffects), Constants.CONSTANT_DOUBLE_TYPE));
		Double utilityGeneralPopulation = (inputParametersDecisionTree.getUtilityGeneralPopulation() != null) ? inputParametersDecisionTree.getUtilityGeneralPopulation() : 1.0;
		DataStoreService.storePropertyDataIntoDiseaseDatacheet(diseaseName, new PropertyData(Constants.DATASHEET_UTILITY_GENERAL_POPULATION, String.valueOf(utilityGeneralPopulation), Constants.CONSTANT_DOUBLE_TYPE));
		
		Map<String, Map<String, PropertyData>> dataProperties = DataStoreService.eTLDataPropertyValues(ontology);
		String prevalenceAtBith = dataProperties.get(diseaseName).get(Constants.DATAPROPERTY_BIRTH_PREVALENCE).getValue().split(Constants.CONSTANT_HASHTAG)[0];
		DataStoreService.storePropertyDataIntoDiseaseDatacheet(diseaseName, new PropertyData(Constants.DATASHEET_PREVALENCE_AT_BITH, prevalenceAtBith, Constants.CONSTANT_STRING_TYPE));

		Double naturalDevelopmentLifeExpectancy = OntologyUtils.calculateDiseaseLifeExpectancy(ontology, diseaseName);
		DataStoreService.storePropertyDataIntoDiseaseDatacheet(diseaseName, new PropertyData(Constants.DATASHEET_NATURAL_DEVELOPMENT_LIFE_EXPECTANCY, String.valueOf(naturalDevelopmentLifeExpectancy), Constants.CONSTANT_DOUBLE_TYPE));
		
		for (String interventionName : interventionsToCompare) {
			if (instancesToClazz.get(interventionName) == null) {
				throw new TranspilerException (String.format("¡¡ ERROR !!. [%s]", "Intervention not found"));
			} else {
				// Calculate the cost of the strategy of follow-up and treatment per intervention
				Double followUpStrategyTotalCost = CostsUtils.calculateInterventionStrategyCost(ontology, interventionName, Constants.OBJECTPROPERTY_INTERVENTION_FOLLOWUP_STRATEGY);
				Double treatmentStrategyTotalCost = CostsUtils.calculateInterventionStrategyCost(ontology, interventionName, Constants.OBJECTPROPERTY_INTERVENTION_TREATMENT_STRATEGY);

				PropertyData followUpTreatmentStrategiesCosts = new PropertyData (Constants.DATASHEET_FOLLOWUP_TREATMENT_STRATEGIES_COSTS, String.format(Locale.US, Constants.CONSTANT_DOUBLE_FORMAT_STRING_3DEC, 
						CostsUtils.calculateDiscount((followUpStrategyTotalCost + treatmentStrategyTotalCost), discountRateCosts, 0.0, naturalDevelopmentLifeExpectancy)), Constants.CONSTANT_DOUBLE_TYPE);
				DataStoreService.storePropertyDataIntoDiseaseDatacheet(diseaseName, followUpTreatmentStrategiesCosts);
				
				/* INTERVENTION NODE */
				TreeNode<NodeData> intervention = root.addChild((new NodeData(interventionName, instancesToClazz.get(interventionName))
						.addProperties(DataStoreService.eTLDataPropertyValues(ontology).get(interventionName))));

				String kindIntervention = dataProperties.get(interventionName).get(Constants.DATAPROPERTY_KIND_INTERVENTION).getValue().split(Constants.CONSTANT_SPLIT_TYPE)[0];
				if (Constants.DATAPROPERTYVALUE_KIND_INTERVENTION_SCREENING_VALUE.equals(kindIntervention)) {
					// ES NECESARIO CALCULAR PARA LA ESTRATEGIA DE CRIBADO LA SENSIBILIDAD Y LA ESPECIFICIDAD
					Double [] sensitivityAndSpecificityScreeningStrategy = OntologyUtils.calculateDiseaseStrategySensitivityAndSpecificity(ontology, diseaseName, interventionName, Constants.OBJECTPROPERTY_INTERVENTION_SCREENING_STRATEGY);
					
					Double screeningStrategyTotalCost = CostsUtils.calculateInterventionStrategyCost(ontology, interventionName, Constants.OBJECTPROPERTY_INTERVENTION_SCREENING_STRATEGY);
					CostsUtils.addCostToNode(intervention, screeningStrategyTotalCost, Constants.DATAPROPERTYVALUE_TEMPORAL_BEHAVIOR_LIFETIME_VALUE);
					
					/* SUB-TREE IF HAS DISEASE */
					TreeNode<NodeData> hasDisease = intervention.addChild(new NodeData("Disease", Constants.CONSTANT_DECISION_NODE)
							.addProperty(Constants.DATAPROPERTY_PROBABILITY, prevalenceAtBith, Constants.CONSTANT_DOUBLE_TYPE));
					CostsUtils.addCostToNode(hasDisease, followUpStrategyTotalCost + treatmentStrategyTotalCost, Constants.DATAPROPERTYVALUE_TEMPORAL_BEHAVIOR_ANNUAL_VALUE);

						TreeNode<NodeData> falseNegative = hasDisease.addChild(new NodeData("False Negative", Constants.CONSTANT_DECISION_NODE)
								.addProperty(Constants.DATAPROPERTY_PROBABILITY, String.valueOf(1.0 - sensitivityAndSpecificityScreeningStrategy[0]), Constants.CONSTANT_DOUBLE_TYPE)
								.addProperty(Constants.CUSTOM_PROPERTY_UTILITY_VALUE_MINIMUM, String.format(Locale.US, Constants.CONSTANT_DOUBLE_FORMAT_STRING_3DEC, utilityGeneralPopulation), Constants.CONSTANT_DOUBLE_TYPE));
						if (1.0 - sensitivityAndSpecificityScreeningStrategy[0] != 0.0) {
							OntologyUtils.loadManifestationsFromNaturalDevelopment(ontology, diseaseName, interventionName, falseNegative, false);
						}
					
						TreeNode<NodeData> truePositive = hasDisease.addChild(new NodeData("True Positive", Constants.CONSTANT_DECISION_NODE)
								.addProperty(Constants.DATAPROPERTY_PROBABILITY, String.valueOf(sensitivityAndSpecificityScreeningStrategy[0]), Constants.CONSTANT_DOUBLE_TYPE)
								.addProperty(Constants.CUSTOM_PROPERTY_UTILITY_VALUE_MINIMUM, String.format(Locale.US, Constants.CONSTANT_DOUBLE_FORMAT_STRING_3DEC, utilityGeneralPopulation), Constants.CONSTANT_DOUBLE_TYPE));
						OntologyUtils.loadClinicalDiagnosisStrategy(ontology, clinicalDiagnosisStrategy, truePositive);
						OntologyUtils.loadManifestationsFromNaturalDevelopment(ontology, diseaseName, interventionName, truePositive, true);

					/* SUB-TREE IF HAS NOT DISEASE */
					TreeNode<NodeData> hasNotDisease = intervention.addChild((new NodeData(Constants.CONSTANT_NEGATION + "Disease", Constants.CONSTANT_DECISION_NODE)
							.addProperty(Constants.DATAPROPERTY_PROBABILITY, prevalenceAtBith, Constants.CONSTANT_DOUBLE_TYPE)));
					
						TreeNode<NodeData> falsePositive = hasNotDisease.addChild(new NodeData("False Positive", Constants.CONSTANT_DECISION_NODE)
								.addProperty(Constants.DATAPROPERTY_PROBABILITY, String.valueOf(1.0 - sensitivityAndSpecificityScreeningStrategy[1]), Constants.CONSTANT_DOUBLE_TYPE)
								.addProperty(Constants.CUSTOM_PROPERTY_UTILITY_VALUE_MINIMUM, String.format(Locale.US, Constants.CONSTANT_DOUBLE_FORMAT_STRING_3DEC, utilityGeneralPopulation), Constants.CONSTANT_DOUBLE_TYPE));
						if (1.0 - sensitivityAndSpecificityScreeningStrategy[1] != 0.0) {
							OntologyUtils.loadClinicalDiagnosisStrategy(ontology, clinicalDiagnosisStrategy, falsePositive);
						}
					
						TreeNode<NodeData> trueNegative = hasNotDisease.addChild(new NodeData("True Negative", Constants.CONSTANT_DECISION_NODE)
								.addProperty(Constants.DATAPROPERTY_PROBABILITY, String.valueOf(sensitivityAndSpecificityScreeningStrategy[1]), Constants.CONSTANT_DOUBLE_TYPE)
								.addProperty(Constants.CUSTOM_PROPERTY_UTILITY_VALUE_MINIMUM, String.format(Locale.US, Constants.CONSTANT_DOUBLE_FORMAT_STRING_3DEC, utilityGeneralPopulation), Constants.CONSTANT_DOUBLE_TYPE));
				} else if (Constants.DATAPROPERTYVALUE_KIND_INTERVENTION_NOSCREENING_VALUE.equals(kindIntervention)) {
					/* SUB-TREE IF HAS DISEASE */
					TreeNode<NodeData> hasDisease = intervention.addChild((new NodeData("Disease", Constants.CONSTANT_DECISION_NODE)
							.addProperty(Constants.DATAPROPERTY_PROBABILITY, prevalenceAtBith, Constants.CONSTANT_DOUBLE_TYPE))
							.addProperty(Constants.CUSTOM_PROPERTY_UTILITY_VALUE_MINIMUM, String.format(Locale.US, Constants.CONSTANT_DOUBLE_FORMAT_STRING_3DEC, utilityGeneralPopulation), Constants.CONSTANT_DOUBLE_TYPE));	
					CostsUtils.addCostToNode(hasDisease, followUpStrategyTotalCost + treatmentStrategyTotalCost, Constants.DATAPROPERTYVALUE_TEMPORAL_BEHAVIOR_ANNUAL_VALUE);
					OntologyUtils.loadManifestationsFromNaturalDevelopment(ontology, diseaseName, interventionName, hasDisease, true);

					/* SUB-TREE IF HAS NOT DISEASE */
					TreeNode<NodeData> hasNotDisease = intervention.addChild((new NodeData(Constants.CONSTANT_NEGATION + "Disease", Constants.CONSTANT_DECISION_NODE)
							.addProperty(Constants.DATAPROPERTY_PROBABILITY, prevalenceAtBith, Constants.CONSTANT_DOUBLE_TYPE))
							.addProperty(Constants.CUSTOM_PROPERTY_UTILITY_VALUE_MINIMUM, String.format(Locale.US, Constants.CONSTANT_DOUBLE_FORMAT_STRING_3DEC, utilityGeneralPopulation), Constants.CONSTANT_DOUBLE_TYPE));
				}				
			}
		}
		
		DecisionTreeNodeProcessor processor = new DecisionTreeNodeProcessor(diseaseName); 
		processor.processProperties(root);
		for (TreeNode<NodeData> intervention : root.getChildren()) {
			Double interventionSummaryQalys = processor.processQalys(intervention);
			Double interventionSummaryCosts = processor.processCosts(intervention);

			PropertyData propertyDataInterventionSummaryQalys = new PropertyData(Constants.DATASHEET_INTERVENTION_SUMMARY_QALYS, String.valueOf(interventionSummaryQalys), Constants.CONSTANT_DOUBLE_TYPE);
			DataStoreService.storePropertyDataIntoDiseaseDatacheet(diseaseName, intervention.getData().getName(), propertyDataInterventionSummaryQalys);
			System.out.println(intervention.getData().getName() + " = " + propertyDataInterventionSummaryQalys.getValue());
			
			PropertyData propertyDataInterventionSummaryCosts = new PropertyData(Constants.DATASHEET_INTERVENTION_SUMMARY_COSTS, String.valueOf(interventionSummaryCosts), Constants.CONSTANT_DOUBLE_TYPE);
			DataStoreService.storePropertyDataIntoDiseaseDatacheet(diseaseName, intervention.getData().getName(), propertyDataInterventionSummaryCosts);
			System.out.println(intervention.getData().getName() + " = " + propertyDataInterventionSummaryCosts.getValue());
		}
		
		return root;
	}

}
