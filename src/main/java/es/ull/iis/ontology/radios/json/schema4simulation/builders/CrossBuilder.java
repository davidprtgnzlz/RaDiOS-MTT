package es.ull.iis.ontology.radios.json.schema4simulation.builders;

import java.util.ArrayList;
import java.util.List;

import es.ull.iis.ontology.radios.Constants;
import es.ull.iis.ontology.radios.json.schema4simulation.Cost;
import es.ull.iis.ontology.radios.json.schema4simulation.Guideline;
import es.ull.iis.ontology.radios.json.schema4simulation.Utility;

public class CrossBuilder {
	public static String recalculatePropabilityField(String manifestationName, String dataPropertyValue, String dataPropertyValueDistro) {
		String mf = "";
		if (OwlHelper.getDataPropertyValue(manifestationName, dataPropertyValue) != null) {
			mf += OwlHelper.getDataPropertyValue(manifestationName, dataPropertyValue);
		} 		
		if (!mf.isEmpty() && OwlHelper.getDataPropertyValue(manifestationName, dataPropertyValueDistro) != null) {
			mf += Constants.CONSTANT_HASHTAG + OwlHelper.getDataPropertyValue(manifestationName, dataPropertyValueDistro);
		} else if (mf.isEmpty() && OwlHelper.getDataPropertyValue(manifestationName, dataPropertyValueDistro) != null) {
			mf += OwlHelper.getDataPropertyValue(manifestationName, dataPropertyValueDistro);
		}
		return !mf.isEmpty() ? mf : null;
	}
	

	public static List<Cost> getCosts(String objectName) {
		List<Cost> result = new ArrayList<>();
		List<String> costs = OwlHelper.getChildsByClassName(objectName, Constants.CLASS_COST);
		for (String costName: costs) {
			Cost cost = new Cost();
			cost.setName(costName);
			cost.setAmount(OwlHelper.getDataPropertyValue(costName, Constants.DATAPROPERTY_AMOUNT));
			cost.setTemporalBehavior(OwlHelper.getDataPropertyValue(costName, Constants.DATAPROPERTY_TEMPORAL_BEHAVIOR));
			cost.setYear(OwlHelper.getDataPropertyValue(costName, Constants.DATAPROPERTY_YEAR));
			result.add(cost);
		}
		return !result.isEmpty() ? result : null;
	}

	public static List<Utility> getUtilities(String objectName) {
		List<Utility> result = new ArrayList<>();
		List<String> utilities = OwlHelper.getChildsByClassName(objectName, Constants.CLASS_UTILITY);
		for (String utilityName: utilities) { 
			Utility utility = new Utility();
			utility.setName(utilityName);
			utility.setKind(OwlHelper.getDataPropertyValue(utilityName, Constants.DATAPROPERTY_KIND_UTILITY));
			String calculatedMethod = OwlHelper.getDataPropertyValue(utilityName, Constants.DATAPROPERTY_CALCULATEMETHOD);
			utility.setCalculationMethod(calculatedMethod != null && !calculatedMethod.isEmpty() ? calculatedMethod : Constants.DATAPROPERTYVALUE_CALCULATED_METHOD_DEFAULT);
			utility.setTemporalBehavior(OwlHelper.getDataPropertyValue(utilityName, Constants.DATAPROPERTY_TEMPORAL_BEHAVIOR));
			utility.setValue(recalculatePropabilityField(utilityName, Constants.DATAPROPERTY_VALUE, Constants.DATAPROPERTY_VALUE_DISTRIBUTION));
			result.add(utility);
		}
		return !result.isEmpty() ? result : null;
	}

	public static List<Guideline> getGuidelines(String objectName) {
		List<Guideline> result = new ArrayList<>();
		List<String> guidelines = OwlHelper.getChildsByClassName(objectName, Constants.CLASS_GUIDELINES);
		for (String guidelineName: guidelines) {
			Guideline guideline = new Guideline();
			guideline.setName(guidelineName);
			guideline.setConditions(OwlHelper.getDataPropertyValue(guidelineName, Constants.DATAPROPERTY_CONDITIONS));
			guideline.setDose(OwlHelper.getDataPropertyValue(guidelineName, Constants.DATAPROPERTY_DOSE));
			guideline.setFrequency(OwlHelper.getDataPropertyValue(guidelineName, Constants.DATAPROPERTY_FREQUENCY));
			guideline.setHoursIntervals(OwlHelper.getDataPropertyValue(guidelineName, Constants.DATAPROPERTY_HOURSINTERVAL));
			guideline.setRange(OwlHelper.getDataPropertyValue(guidelineName, Constants.DATAPROPERTY_RANGE));
			guideline.setUtilities(getUtilities(guidelineName));
			result.add(guideline);
		}
		return !result.isEmpty() ? result : null;
	}
}
