package es.ull.iis.ontology.radios.json.databind;

public class ConnectorsResource {
	private String type;
	private StyleResource style;

	public ConnectorsResource() {
	}

	public ConnectorsResource(String type) {
		this.type = type;
	}

	public ConnectorsResource(String type, StyleResource style) {
		this.type = type;
		this.style = style;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public StyleResource getStyle() {
		return style;
	}

	public void setStyle(StyleResource style) {
		this.style = style;
	}
}
