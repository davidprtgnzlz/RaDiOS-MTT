package es.ull.iis.ontology.radios.json.schema4simulation.builders;

import java.util.ArrayList;
import java.util.List;

import es.ull.iis.ontology.radios.Constants;
import es.ull.iis.ontology.radios.json.schema4simulation.ClinicalDiagnosisStrategy;
import es.ull.iis.ontology.radios.json.schema4simulation.ClinicalDiagnosisTechnique;

public class ClinicalDiagnosisBuilder {
	public static List<ClinicalDiagnosisStrategy> getClinicalDiagnosisStrategies(String objectName) {
		List<ClinicalDiagnosisStrategy> result = new ArrayList<>();
		List<String> clinicalDiagnosisStrategies = OwlHelper.getChildsByClassName(objectName, Constants.CLASS_CLINICALDIAGNOSISSTRATEGY);
		for (String clinicalDiagnosisStrategyName: clinicalDiagnosisStrategies) {
			ClinicalDiagnosisStrategy clinicalDiagnosisStrategy = new ClinicalDiagnosisStrategy();
			clinicalDiagnosisStrategy.setName(clinicalDiagnosisStrategyName);
			clinicalDiagnosisStrategy.setCosts(CrossBuilder.getCosts(clinicalDiagnosisStrategyName));
			clinicalDiagnosisStrategy.setClinicalDiagnosisTechniques(getClinicalDiagnosisTechniques(clinicalDiagnosisStrategyName));
			result.add(clinicalDiagnosisStrategy);
		}
		return !result.isEmpty() ? result : null;
	}
	
	public static List<ClinicalDiagnosisTechnique> getClinicalDiagnosisTechniques(String clinicalDiagnosisStrategyName) {
		List<ClinicalDiagnosisTechnique> result = new ArrayList<>();
		List<String> clinicalDiagnosisStepsStrategy = OwlHelper.getChildsByClassName(clinicalDiagnosisStrategyName, Constants.CLASS_CLINICALDIAGNOSISSTEPSTRATEGY);
		for (String clinicalDiagnosisStepStrategyName: clinicalDiagnosisStepsStrategy) {
			List<String> clinicalDiagnosisTechniques = OwlHelper.getChildsByClassName(clinicalDiagnosisStepStrategyName, Constants.CLASS_CLINICALDIAGNOSIS);
			for (String clinicalDiagnosisTechniqueName: clinicalDiagnosisTechniques) {
				result.add(getClinicalDiagnosisTechnique(clinicalDiagnosisTechniqueName, OwlHelper.getDataPropertyValue(clinicalDiagnosisStepStrategyName, Constants.DATAPROPERTY_PERCENTFORTHENEXT)));
			}
		}
		return !result.isEmpty() ? result : null;
	}
	
	public static ClinicalDiagnosisTechnique getClinicalDiagnosisTechnique(String clinicalDiagnosisTechniqueName, String percentForTheNext) {
		ClinicalDiagnosisTechnique clinicalDiagnosis = new ClinicalDiagnosisTechnique();
		clinicalDiagnosis.setName(clinicalDiagnosisTechniqueName);
		clinicalDiagnosis.setCosts(CrossBuilder.getCosts(clinicalDiagnosisTechniqueName));
		clinicalDiagnosis.setEspecificity(OwlHelper.getDataPropertyValue(clinicalDiagnosisTechniqueName, Constants.DATAPROPERTY_SPECIFICITY));
		clinicalDiagnosis.setPercentageNext(percentForTheNext);
		clinicalDiagnosis.setSensitivity(OwlHelper.getDataPropertyValue(clinicalDiagnosisTechniqueName, Constants.DATAPROPERTY_SENSITIVITY));
		return clinicalDiagnosis;
	}
}
