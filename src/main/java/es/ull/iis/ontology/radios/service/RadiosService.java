package es.ull.iis.ontology.radios.service;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;

import org.apache.commons.lang3.StringUtils;
import org.w3c.xsd.owl2.Ontology;

import com.beust.jcommander.JCommander;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import es.ull.iis.ontology.radios.Constants;
import es.ull.iis.ontology.radios.NodeData;
import es.ull.iis.ontology.radios.TreeNode;
import es.ull.iis.ontology.radios.exceptions.TranspilerException;
import es.ull.iis.ontology.radios.json.cytoscape.Cytoscape;
import es.ull.iis.ontology.radios.json.cytoscape.Data;
import es.ull.iis.ontology.radios.json.cytoscape.Edge;
import es.ull.iis.ontology.radios.json.cytoscape.Node;
import es.ull.iis.ontology.radios.json.databind.ChartResource;
import es.ull.iis.ontology.radios.json.databind.ConnectorsResource;
import es.ull.iis.ontology.radios.json.databind.InputParametersDecisionTree;
import es.ull.iis.ontology.radios.json.databind.InputParametersDesSimulation;
import es.ull.iis.ontology.radios.json.databind.NodeResource;
import es.ull.iis.ontology.radios.json.databind.NodeStructureResource;
import es.ull.iis.ontology.radios.json.databind.ParameterResource;
import es.ull.iis.ontology.radios.json.databind.RadiosMTTPshigosResource;
import es.ull.iis.ontology.radios.json.databind.RadiosPsighosSimResult;
import es.ull.iis.ontology.radios.json.databind.StyleResource;
import es.ull.iis.ontology.radios.json.databind.TreantTreeStructureResource;
import es.ull.iis.ontology.radios.json.linechart.Layout;
import es.ull.iis.ontology.radios.json.linechart.Linechart;
import es.ull.iis.ontology.radios.json.linechart.Linecharts;
import es.ull.iis.ontology.radios.json.linechart.Trace;
import es.ull.iis.ontology.radios.json.linechart.Xaxis;
import es.ull.iis.ontology.radios.json.linechart.Yaxis;
import es.ull.iis.ontology.radios.json.schema4simulation.Schema4Simulation;
import es.ull.iis.ontology.radios.json.schema4simulation.builders.DiseaseBuilder;
import es.ull.iis.ontology.radios.json.schema4simulation.builders.OwlHelper;
import es.ull.iis.ontology.radios.utils.CollectionUtils;
import es.ull.iis.ontology.radios.utils.TreantTreeUtils;
import es.ull.iis.ontology.radios.utils.TreeUtils;
import es.ull.iis.ontology.radios.wrappers.IntegerWrapper;
import es.ull.iis.simulation.hta.DiseaseMain;
import es.ull.iis.simulation.hta.DiseaseMain.Arguments;
import es.ull.iis.simulation.hta.params.BasicConfigParams;
import es.ull.iis.simulation.hta.progression.Manifestation.Type;
import es.ull.iis.simulation.hta.progression.Transition;
import es.ull.iis.simulation.hta.radios.RadiosExperimentResult;
import es.ull.iis.simulation.hta.radios.exceptions.TransformException;

public class RadiosService {
	/**
	 * @param radios
	 * @param disease
	 * @param interventionA
	 * @param interventionB
	 * @param clinicalDiagnosisStrategyParam
	 * @return
	 * @throws TranspilerException
	 * @throws IOException
	 */
	public static String buildPlainDecisionTree(Ontology radios, String disease, String interventionA, String interventionB, String clinicalDiagnosisStrategyParam) throws TranspilerException, IOException {
		String diseaseName = (disease.startsWith("#")) ? disease : "#" + disease;
		
		String clinicalDiagnosisStrategy = "#PBD_ClinicalDiagnosisStrategy";
		if (clinicalDiagnosisStrategyParam != null) {
			clinicalDiagnosisStrategy = clinicalDiagnosisStrategyParam.startsWith("#") ? clinicalDiagnosisStrategyParam : "#" + clinicalDiagnosisStrategyParam;
		}
		
		List<String> interventionsToCompare = new ArrayList<>();
		interventionsToCompare.add((interventionA.startsWith("#")) ? interventionA : "#" + interventionA);
		interventionsToCompare.add((interventionB.startsWith("#")) ? interventionB : "#" + interventionB);
		
		TreeNode<NodeData> decisionTree;
		ByteArrayOutputStream baos = new ByteArrayOutputStream ();		
		InputParametersDecisionTree inputParametersDecisionTree = new InputParametersDecisionTree ();
		inputParametersDecisionTree.setDisease(diseaseName);
		inputParametersDecisionTree.setInterventions(interventionsToCompare);
		inputParametersDecisionTree.setClinicalDiagnosisStrategy(clinicalDiagnosisStrategy);
		decisionTree = DecisionTreeBuilderService.buildDecisionTreeFromOntology(radios, inputParametersDecisionTree);
		TreeUtils.inorden(decisionTree, Constants.CONSTANT_EMPTY_STRING, baos);

		return new String (baos.toByteArray());
	}

	/**
	 * @param radios
	 * @param inputParameters
	 * @return
	 * @throws TranspilerException
	 * @throws IOException
	 */
	public static TreantTreeStructureResource buildTreantDecisionTree(Ontology radios, InputParametersDecisionTree inputParameters) throws TranspilerException, IOException {
		TreantTreeStructureResource treeStructure = new TreantTreeStructureResource();

		treeStructure.setChart(new ChartResource("#DecisionTree", 10, 20, 20, "WEST"));
		treeStructure.getChart().setNode(new NodeResource("decisiontree-draw", true));
		treeStructure.getChart().setConnectors(new ConnectorsResource("straight", new StyleResource(2, "#777", null, null)));

		TreeNode<NodeData> root = DecisionTreeBuilderService.buildDecisionTreeFromOntology(radios, inputParameters);
		NodeStructureResource rootTreant = NodeStructureResource.toResource(root); 
		
		IntegerWrapper maxLevel = new IntegerWrapper();
		TreantTreeUtils.inorden(root, rootTreant, 0, maxLevel);
		treeStructure.setNodeStructure(rootTreant);
		
		TreantTreeUtils.completeChildsToMaxLevelTree(rootTreant, 0, maxLevel.value);
		
		return treeStructure;
	}

	/**
	 * @param radios
	 * @param inputParameters
	 * @return
	 * @throws TranspilerException
	 * @throws IOException
	 * @throws JAXBException 
	 * @throws TransformException 
	 */
	public static RadiosMTTPshigosResource desSimulation(Ontology radios, InputParametersDesSimulation inputParameters) throws TranspilerException, IOException, TransformException, JAXBException {		
		RadiosMTTPshigosResource radiosMTTPshigosResource = new RadiosMTTPshigosResource();		
		Cytoscape cyJson = new Cytoscape();
		radiosMTTPshigosResource.setCyJson(cyJson);
		
		final Arguments arguments = new Arguments();
		JCommander jc = JCommander.newBuilder().addObject(arguments).build();
		String params = String.format("-n %s -r %s -dr %s -h %s -q -pop %s -dis %s -ep ia", 
				inputParameters.getNumPatients(), inputParameters.getNumSimulations(), inputParameters.getDiscountRateCosts() + " " + inputParameters.getDiscountRateEffects(), 
				inputParameters.getTimeHorizon(), "1", "11");
		jc.parse(params.split(" "));
		BasicConfigParams.STUDY_YEAR = arguments.year;

		for (final Map.Entry<String, String> pInit : arguments.initProportions.entrySet()) {
			BasicConfigParams.INIT_PROP.put(pInit.getKey(), Double.parseDouble(pInit.getValue()));
		}

		String diseaseName = inputParameters.getDisease();
		OwlHelper.initilize(radios);
		ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT).setSerializationInclusion(Include.NON_NULL).setSerializationInclusion(Include.NON_EMPTY);
		Schema4Simulation radiosDiseaseInstance = mapper.readValue(mapper.writeValueAsString(new Schema4Simulation(DiseaseBuilder.getDisease(diseaseName))).getBytes(), Schema4Simulation.class);			
		
		RadiosExperimentResult result = DiseaseMain.runExperiment(arguments, radiosDiseaseInstance, inputParameters.getInterventions(), 
				inputParameters.getAllPatientsAffected(), inputParameters.getUtilityGeneralPopulation());

		System.out.println("=====================================================================================================");
		System.out.println((new String (result.getSimResult().toByteArray())));
		System.out.println("=====================================================================================================");
		
		
		createDiseaseCytescapeGraph(cyJson, result);
		createSecondOrderParametersResource(radiosMTTPshigosResource, result);			
		createDiseaseIncidenceLineChart(radiosMTTPshigosResource, result, inputParameters.getNumSimulations());
		createSimulationResultResource(radiosMTTPshigosResource, result, inputParameters.getNumSimulations());

		return radiosMTTPshigosResource;
	}

	/**
	 * @param radiosMTTPshigosResource
	 * @param result
	 */
	private static void createSimulationResultResource(RadiosMTTPshigosResource radiosMTTPshigosResource, RadiosExperimentResult result, int numSimulations) {
		RadiosPsighosSimResult radiosPsighosSimResult = new RadiosPsighosSimResult();
		String simulationResult = new String(result.getSimResult().toByteArray());
		String[] lines = simulationResult.split("\n");
		if (lines != null && lines.length > 0 && !StringUtils.isEmpty(lines[0])) {
			if (lines[0] != null) {
				radiosPsighosSimResult.getDeterministic().setHeaders(Arrays.asList(lines[0].split("\t")));
			}
			if (lines[1] != null) {
				radiosPsighosSimResult.getDeterministic().getValues().add(CollectionUtils.transformDoubleValueStringListToFormattedStringList(Arrays.asList(lines[1].split("\t")), 1));
			}

			if (lines[2] != null && !lines[2].startsWith("#####")) {
				radiosPsighosSimResult.getProbabilistic().setHeaders(Arrays.asList(lines[2].split("\t")));
				for (int i = 3; i < numSimulations + 3; i++) {
					radiosPsighosSimResult.getProbabilistic().getValues().add(CollectionUtils.transformDoubleValueStringListToFormattedStringList(Arrays.asList(lines[i].split("\t")), 1));
				}
				
				if (!radiosPsighosSimResult.getProbabilistic().getValues().isEmpty()) {
					Integer rowSize = radiosPsighosSimResult.getProbabilistic().getValues().size();
					Integer colSize = radiosPsighosSimResult.getProbabilistic().getValues().get(0).size();
					List<Double> avg = CollectionUtils.createInitializeDoubleList(colSize, 0.0);
					List<Double> min = CollectionUtils.createInitializeDoubleList(colSize, Double.MAX_VALUE);
					List<Double> max = CollectionUtils.createInitializeDoubleList(colSize, 0.0);
					for (List<String> row : radiosPsighosSimResult.getProbabilistic().getValues()) {
						for (int i = 1; i < row.size(); i++) {
							Double value = Double.parseDouble(row.get(i).replace(",", "."));
							if (value < min.get(i)) {
								min.set(i, value);							
							}
							if (value > max.get(i)) {
								max.set(i, value);							
							}
							avg.set(i, avg.get(i) + value);
						}					
					}
					for (int i = 1; i < avg.size(); i++) {
						avg.set(i, avg.get(i) / rowSize);
					}

					List<Double> stdDev = CollectionUtils.createInitializeDoubleList(colSize, 0.0);
					for (int i = 1; i < colSize; i++) {
						for (int j = 0; j < rowSize; j++) {
							Double value = Double.parseDouble(radiosPsighosSimResult.getProbabilistic().getValues().get(j).get(i).replace(",", "."));
							stdDev.set(i, stdDev.get(i) + Math.pow(value - avg.get(i), 2));
						}
						stdDev.set(i, Math.sqrt(stdDev.get(i) / (double) rowSize));
					}

					List<String> list = radiosPsighosSimResult.getProbabilistic().getHeaders();
					list.set(0, "FUNC");
					radiosPsighosSimResult.getSummaryProbabilistic().setHeaders(list);
					
					list = CollectionUtils.transformDoubleListToFormattedStringList(avg, 1); 
					list.set(0, "AVG");
					radiosPsighosSimResult.getSummaryProbabilistic().getValues().add(list);
					
					list = CollectionUtils.transformDoubleListToFormattedStringList(stdDev, 1); 
					list.set(0, "SDEV");
					radiosPsighosSimResult.getSummaryProbabilistic().getValues().add(list);

					list = CollectionUtils.transformDoubleListToFormattedStringList(min, 1); 
					list.set(0, "MIN");
					radiosPsighosSimResult.getSummaryProbabilistic().getValues().add(list);
					
					list = CollectionUtils.transformDoubleListToFormattedStringList(max, 1); 
					list.set(0, "MAX");
					radiosPsighosSimResult.getSummaryProbabilistic().getValues().add(list);
				}
			}
		}
		radiosMTTPshigosResource.setSimulationResult(radiosPsighosSimResult);
	}

	/**
	 * @param simulationResult
	 * @param result
	 */
	private static void createSecondOrderParametersResource(RadiosMTTPshigosResource simulationResult, RadiosExperimentResult result) {
		for (String parameter : result.getPrettySavedParams().split("\n")) {
			String[] parameterSplited = parameter.split(":::");
			simulationResult.getParameters().add(new ParameterResource(parameterSplited[0], parameterSplited[1], parameterSplited[2]));
		}
	}

	/**
	 * @param cyJson
	 * @param result
	 */
	private static void createDiseaseCytescapeGraph(Cytoscape cyJson, RadiosExperimentResult result) {
		Map<String, String> nodes = new HashMap<>();
		for (Transition transition : result.getTransitions()) {
			nodes.put(transition.getSrcManifestation().getName(), transition.getSrcManifestation().getType().name());
			nodes.put(transition.getDestManifestation().getName(), transition.getDestManifestation().getType().name());
		}
		
		Integer i = 0;
		Map<String, String> nodesWithIds = new HashMap<>();
		for (String node : nodes.keySet()) {
			nodesWithIds.put(node, (++i).toString());
			cyJson.getNodes().add(new Node(new Data((i).toString(), node, null, null, nodes.get(node), false)));			
		}
		for (Transition transition : result.getTransitions()) {
			cyJson.getEdges().add(new Edge(new Data(null, null, nodesWithIds.get(transition.getSrcManifestation().getName()), nodesWithIds.get(transition.getDestManifestation().getName()), null, false)));
			if (transition.replacesPrevious()) {
				for (Node node: cyJson.getNodes()) {
					if (node.getData().getName().equalsIgnoreCase(transition.getDestManifestation().getName())) {
						node.getData().setReplacePrevious(true);
						break;
					}					
				}
			}
			if (Type.ACUTE.name().equalsIgnoreCase(transition.getDestManifestation().getType().name())) {
				cyJson.getEdges().add(new Edge(new Data(null, null, nodesWithIds.get(transition.getDestManifestation().getName()), nodesWithIds.get(transition.getSrcManifestation().getName()), null, false)));
			}
		}
		for (Node node : cyJson.getNodes()) {
			if ("NONE".equalsIgnoreCase(node.getData().getName())) {
				node.getData().setName("HEALTHY");
				node.getData().setType("HEALTHY");
			}
		}
	}
	
	public static String getFileContent(FileInputStream fis, String encoding) throws IOException {
		try (BufferedReader br = new BufferedReader(new InputStreamReader(fis, encoding))) {
			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = br.readLine()) != null) {
				sb.append(line);
				sb.append('\n');
			}
			return sb.toString();
		}
	}
	
	private static String createDiseaseIncidenceLineChart(RadiosMTTPshigosResource simulationResult, RadiosExperimentResult result, int numSimulations) throws JsonProcessingException {		
		String[] original = new String(result.getSimResult().toByteArray()).split("\n");

		// De la cadena de caracteres del resultado de simulación, quitamos las 2 primeras líneas correspondientes al resultado determinístico.
		// Si hubieramos seleccionado simulación probabilística, tendriamos que quitar además la línea de cabecera de este resultado más otras  
		// correspondientes al número de simulaciones.
		if (original.length > (numSimulations + 1) + 2) {
			List<List<String>> incidenceTables = calculateIncidenceTables(numSimulations, original);
			if (CollectionUtils.notIsEmpty(incidenceTables)) {
				Linecharts linecharts = new Linecharts(new ArrayList<>());
				for (List<String> incidenceTable : incidenceTables) {
					String [] tmp = incidenceTable.toArray(new String[0]);
					Double[][] matrixIncidence = calculateMatrixIncidence(Arrays.copyOfRange(tmp, 2, tmp.length));

					List<Double> x_axi = new ArrayList<>();
					for (int i = 0; i < matrixIncidence.length; i++) {
						x_axi.add(matrixIncidence[i][0]);
					}					
					
					String[] headers = tmp[1].split("\t");
					List<Trace> traceList = new ArrayList<>();
					for (int i = 1; i < matrixIncidence[0].length; i++) {
						List<Double> y_axi = new ArrayList<>();
						for (int j = 0; j < matrixIncidence.length; j++) {
							y_axi.add(matrixIncidence[j][i]);
						}				
						traceList.add(new Trace(x_axi, y_axi, "scatter", headers[i], "lines+markers"));
					}
					Linechart linechart = new Linechart(new Layout(String.format("Incidence Line Chart for Disease [%s]", tmp[0]), new Xaxis("Age", false, false), new Yaxis("", false)),traceList);
					linecharts.getLinechart().add(linechart);
				}			
				
				simulationResult.setLinecharts(linecharts);

				ObjectMapper mapper = new ObjectMapper()
						.enable(SerializationFeature.INDENT_OUTPUT)
						.setSerializationInclusion(Include.NON_NULL)
						.setSerializationInclusion(Include.NON_EMPTY)
						.configure(SerializationFeature.WRITE_EMPTY_JSON_ARRAYS, false);
				String jsonResult = mapper.writeValueAsString(linecharts);
				System.out.println(jsonResult);
				return jsonResult;
			}
		}
		return Constants.CONSTANT_EMPTY_JSON;
	}

	/**
	 * @param numSimulations
	 * @param original
	 * @return
	 */
	private static List<List<String>> calculateIncidenceTables(int numSimulations, String[] original) {
		List<List<String>> incidenceTables = new ArrayList<>(); 
		List<String> lines = null; 
		String[] denormalizedLines = Arrays.copyOfRange(original, (numSimulations > 0) ? numSimulations + 3 : 2, original.length);
		for (String line : denormalizedLines) {
			String normalizeLine = line.trim().replaceAll("^Incidence ABS$", "").replaceAll("^\t$", "\n").replaceAll("\r", "").replaceAll("\n", "").replaceAll("^-*-$", "");
			if (!StringUtils.isEmpty(normalizeLine)) {
				if (normalizeLine.startsWith("#####")) {
					lines = new ArrayList<>();
					incidenceTables.add(lines);
				} else {
					lines.add(line);
				}
			}
		}
		return incidenceTables;
	}

	/**
	 * @param lines
	 * @return
	 */
	private static Double[][] calculateMatrixIncidence(String[] lines) {
		List<double[]> matrixIncidence = new ArrayList<>();
		for (int i = 0; i < lines.length; i++) {
			matrixIncidence.add(Arrays.stream(lines[i].split("\t")).mapToDouble(Double::parseDouble).toArray());
		}
		Double[][] result = new Double[matrixIncidence.size()][matrixIncidence.get(0).length];
		for (int i = 0; i < matrixIncidence.size(); i++) {
			for (int j = 0; j < matrixIncidence.get(i).length; j++) {
				result[i][j] = matrixIncidence.get(i)[j];
			}
		}
		return result;
	}
}
