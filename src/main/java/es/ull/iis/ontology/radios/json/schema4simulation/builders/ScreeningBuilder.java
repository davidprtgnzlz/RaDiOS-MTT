package es.ull.iis.ontology.radios.json.schema4simulation.builders;

import java.util.ArrayList;
import java.util.List;

import es.ull.iis.ontology.radios.Constants;
import es.ull.iis.ontology.radios.json.schema4simulation.ScreeningStrategy;
import es.ull.iis.ontology.radios.json.schema4simulation.ScreeningTechnique;

public class ScreeningBuilder {
	public static List<ScreeningStrategy> getScreeningStrategies(String objectName) {
		List<ScreeningStrategy> result = new ArrayList<>();
		List<String> screeningStrategies = OwlHelper.getChildsByClassName(objectName, Constants.CLASS_SCREENINGSTRATEGY);
		for (String screeningStrategyName: screeningStrategies) {
			ScreeningStrategy screeningStrategy = new ScreeningStrategy();
			screeningStrategy.setName(screeningStrategyName);
			screeningStrategy.setCosts(CrossBuilder.getCosts(screeningStrategyName));
			screeningStrategy.setScreeningTechniques(getScreeningTechniques(screeningStrategyName));
			result.add(screeningStrategy);
		}
		return !result.isEmpty() ? result : null;
	}
	
	public static List<ScreeningTechnique> getScreeningTechniques(String screeningStrategyName) {
		List<ScreeningTechnique> result = new ArrayList<>();
		List<String> screeningStepsStrategy = OwlHelper.getChildsByClassName(screeningStrategyName, Constants.CLASS_SCREENINGSTEPSTRATEGY);
		for (String screeningStepStrategyName: screeningStepsStrategy) {
			List<String> screeningTechniques = OwlHelper.getChildsByClassName(screeningStepStrategyName, Constants.CLASS_SCREENING);
			for (String screeningTechniqueName: screeningTechniques) {
				result.add(getScreeningTechnique(screeningTechniqueName, OwlHelper.getDataPropertyValue(screeningStepStrategyName, Constants.DATAPROPERTY_PERCENTFORTHENEXT)));
			}
		}
		return !result.isEmpty() ? result : null;
	}
	
	public static ScreeningTechnique getScreeningTechnique(String screeningTechniqueName, String percentForTheNext) {
		ScreeningTechnique screening = new ScreeningTechnique();
		screening.setName(screeningTechniqueName);
		screening.setCosts(CrossBuilder.getCosts(screeningTechniqueName));
		screening.setEspecificity(OwlHelper.getDataPropertyValue(screeningTechniqueName, Constants.DATAPROPERTY_SPECIFICITY));
		screening.setPercentageNext(percentForTheNext);
		screening.setSensitivity(OwlHelper.getDataPropertyValue(screeningTechniqueName, Constants.DATAPROPERTY_SENSITIVITY));
		screening.setTemporaryThreshold(OwlHelper.getDataPropertyValue(screeningTechniqueName, Constants.DATAPROPERTY_TEMPORAL_BEHAVIOR));
		return screening;
	}
}
