package es.ull.iis.ontology.radios.json.databind;

public class ChartResource {
	private String container;
	private Integer levelSeparation;
	private Integer siblingSeparation;
	private Integer subTeeSeparation;
	private String rootOrientation;
	private NodeResource node;
	private ConnectorsResource connectors;

	public ChartResource() {
	}

	public ChartResource(String container, Integer levelSeparation, Integer siblingSeparation, Integer subTeeSeparation, String rootOrientation) {
		this.container = container;
		this.levelSeparation = levelSeparation;
		this.siblingSeparation = siblingSeparation;
		this.subTeeSeparation = subTeeSeparation;
		this.rootOrientation = rootOrientation;
	}

	public ChartResource(String container, Integer levelSeparation, Integer siblingSeparation, Integer subTeeSeparation, String rootOrientation, NodeResource node, ConnectorsResource connectors) {
		this.container = container;
		this.levelSeparation = levelSeparation;
		this.siblingSeparation = siblingSeparation;
		this.subTeeSeparation = subTeeSeparation;
		this.rootOrientation = rootOrientation;
		this.node = node;
		this.connectors = connectors;
	}

	public String getContainer() {
		return container;
	}

	public void setContainer(String container) {
		this.container = container;
	}

	public Integer getLevelSeparation() {
		return levelSeparation;
	}

	public void setLevelSeparation(Integer levelSeparation) {
		this.levelSeparation = levelSeparation;
	}

	public Integer getSiblingSeparation() {
		return siblingSeparation;
	}

	public void setSiblingSeparation(Integer siblingSeparation) {
		this.siblingSeparation = siblingSeparation;
	}

	public Integer getSubTeeSeparation() {
		return subTeeSeparation;
	}

	public void setSubTeeSeparation(Integer subTeeSeparation) {
		this.subTeeSeparation = subTeeSeparation;
	}

	public String getRootOrientation() {
		return rootOrientation;
	}

	public void setRootOrientation(String rootOrientation) {
		this.rootOrientation = rootOrientation;
	}

	public NodeResource getNode() {
		return node;
	}

	public void setNode(NodeResource node) {
		this.node = node;
	}

	public ConnectorsResource getConnectors() {
		return connectors;
	}

	public void setConnectors(ConnectorsResource connectors) {
		this.connectors = connectors;
	}

}
