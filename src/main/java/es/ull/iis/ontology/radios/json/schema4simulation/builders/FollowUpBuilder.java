package es.ull.iis.ontology.radios.json.schema4simulation.builders;

import java.util.ArrayList;
import java.util.List;

import es.ull.iis.ontology.radios.Constants;
import es.ull.iis.ontology.radios.json.schema4simulation.FollowUp;
import es.ull.iis.ontology.radios.json.schema4simulation.FollowUpStrategy;

public class FollowUpBuilder {
	public static List<FollowUpStrategy> getFollowUpStrategies(String objectName) {
		List<FollowUpStrategy> result = new ArrayList<>();
		List<String> followUpStrategies = OwlHelper.getChildsByClassName(objectName, Constants.CLASS_FOLLOWUPSTRATEGY);
		for (String followUpStrategyName: followUpStrategies) {
			FollowUpStrategy followUpStrategy = new FollowUpStrategy();
			followUpStrategy.setName(followUpStrategyName);
			followUpStrategy.setCosts(CrossBuilder.getCosts(followUpStrategyName));
			followUpStrategy.setGuidelines(CrossBuilder.getGuidelines(followUpStrategyName));
			followUpStrategy.setFollowUps(getFollowUps(followUpStrategyName));
			result.add(followUpStrategy);
		}
		return !result.isEmpty() ? result : null;
	}
	
	public static List<FollowUp> getFollowUps(String followUpStrategyName) {
		List<FollowUp> result = new ArrayList<>();
		List<String> followUpStepsStrategy = OwlHelper.getChildsByClassName(followUpStrategyName, Constants.CLASS_FOLLOWUPSTEPSTRATEGY);
		for (String followUpStepStrategyName: followUpStepsStrategy) {
			List<String> followUps = OwlHelper.getChildsByClassName(followUpStepStrategyName, Constants.CLASS_FOLLOWUP);
			for (String followUpName: followUps) {
				result.add(getFollowUp(followUpName));
			}
		}
		return !result.isEmpty() ? result : null;
	}
	
	public static FollowUp getFollowUp(String followUpName) {
		FollowUp followUp = new FollowUp();
		followUp.setName(followUpName);
		followUp.setCosts(CrossBuilder.getCosts(followUpName));
		followUp.setGuidelines(CrossBuilder.getGuidelines(followUpName));
		followUp.setPercentageNext(OwlHelper.getDataPropertyValue(followUpName, Constants.DATAPROPERTY_PERCENTFORTHENEXT));
		return followUp;
	}
	
}
