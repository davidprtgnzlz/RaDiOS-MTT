package es.ull.iis.ontology.radios.json.databind;

import java.util.ArrayList;
import java.util.List;

public class SimulationResultResource {
	private List<String> headers;
	private List<List<String>> values;

	public SimulationResultResource() {
	}

	public SimulationResultResource(List<String> headers, List<List<String>> values) {
		super();
		this.headers = headers;
		this.values = values;
	}

	public List<String> getHeaders() {
		return headers;
	}

	public void setHeaders(List<String> headers) {
		this.headers = headers;
	}

	public List<List<String>> getValues() {
		if (values == null) {
			values = new ArrayList<List<String>>();
		}
		return values;
	}

	public void setValues(List<List<String>> values) {
		this.values = values;
	}

}
