package es.ull.iis.ontology.radios.json.databind;

public class RadiosPsighosSimResult {
	private SimulationResultResource deterministic;
	private SimulationResultResource summaryProbabilistic;
	private SimulationResultResource probabilistic;

	public SimulationResultResource getDeterministic() {
		if (deterministic == null) {
			deterministic = new SimulationResultResource();
		}
		return deterministic;
	}

	public void setDeterministic(SimulationResultResource deterministic) {
		this.deterministic = deterministic;
	}

	public SimulationResultResource getProbabilistic() {
		if (probabilistic == null) {
			probabilistic = new SimulationResultResource();
		}
		return probabilistic;
	}

	public void setProbabilistic(SimulationResultResource probabilistic) {
		this.probabilistic = probabilistic;
	}

	public SimulationResultResource getSummaryProbabilistic() {
		if (summaryProbabilistic == null) {
			summaryProbabilistic = new SimulationResultResource();
		}
		return summaryProbabilistic;
	}

	public void setSummaryProbabilistic(SimulationResultResource summaryProbabilistic) {
		this.summaryProbabilistic = summaryProbabilistic;
	}

}
