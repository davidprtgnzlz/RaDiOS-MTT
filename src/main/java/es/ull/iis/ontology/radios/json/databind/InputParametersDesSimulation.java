package es.ull.iis.ontology.radios.json.databind;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class InputParametersDesSimulation {
	private String disease;
	private List<String> interventions;
	private String clinicalDiagnosisStrategy;
	private Double discountRateCosts;
	private Double discountRateEffects;
	private Double utilityGeneralPopulation;
	private Integer timeHorizon;
	private Integer numSimulations;
	private Integer numPatients;
	private Boolean allPatientsAffected;

	public String getDisease() {
		return disease;
	}

	public void setDisease(String disease) {
		this.disease = disease;
	}

	public List<String> getInterventions() {
		return interventions;
	}

	public void setInterventions(List<String> interventions) {
		this.interventions = interventions;
	}

	public String getClinicalDiagnosisStrategy() {
		return clinicalDiagnosisStrategy;
	}

	public void setClinicalDiagnosisStrategy(String clinicalDiagnosisStrategy) {
		this.clinicalDiagnosisStrategy = clinicalDiagnosisStrategy;
	}

	public Integer getTimeHorizon() {
		return timeHorizon;
	}

	public void setTimeHorizon(Integer timeHorizon) {
		this.timeHorizon = timeHorizon;
	}

	public Integer getNumSimulations() {
		return numSimulations;
	}

	public void setNumSimulations(Integer numSimulations) {
		this.numSimulations = numSimulations;
	}

	public Integer getNumPatients() {
		return numPatients;
	}

	public void setNumPatients(Integer numPatients) {
		this.numPatients = numPatients;
	}

	public Double getDiscountRateCosts() {
		return discountRateCosts;
	}

	public void setDiscountRateCosts(Double discountRateCosts) {
		this.discountRateCosts = discountRateCosts;
	}

	public Double getDiscountRateEffects() {
		return discountRateEffects;
	}

	public void setDiscountRateEffects(Double discountRateEffects) {
		this.discountRateEffects = discountRateEffects;
	}

	public Double getUtilityGeneralPopulation() {
		return utilityGeneralPopulation;
	}

	public void setUtilityGeneralPopulation(Double utilityGeneralPopulation) {
		this.utilityGeneralPopulation = utilityGeneralPopulation;
	}

	public Boolean getAllPatientsAffected() {
		return allPatientsAffected;
	}

	public void setAllPatientsAffected(Boolean allPatientsAffected) {
		this.allPatientsAffected = allPatientsAffected;
	}

	public static void main(String[] args) throws JsonProcessingException {
		InputParametersDesSimulation inputParametersDesSim = new InputParametersDesSimulation();
		inputParametersDesSim.setClinicalDiagnosisStrategy("PBD_ClinicalDiagnosisStrategy");
		inputParametersDesSim.setDiscountRateCosts(0.03);
		inputParametersDesSim.setDiscountRateEffects(0.03);
		inputParametersDesSim.setUtilityGeneralPopulation(1.0);
		inputParametersDesSim.setDisease("PBD_ProfoundBiotinidaseDeficiency");
		inputParametersDesSim.setNumPatients(100);
		inputParametersDesSim.setNumSimulations(1000);
		inputParametersDesSim.setTimeHorizon(10);
		inputParametersDesSim.setAllPatientsAffected(true);
		List<String> interventions = new ArrayList<>();
		interventions.add("PBD_InterventionNoScreening");
		interventions.add("PBD_InterventionScreening");
		inputParametersDesSim.setInterventions(interventions);

		ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT).setSerializationInclusion(Include.NON_NULL).setSerializationInclusion(Include.NON_EMPTY);
		System.out.println(mapper.writeValueAsString(inputParametersDesSim));
	}

}
