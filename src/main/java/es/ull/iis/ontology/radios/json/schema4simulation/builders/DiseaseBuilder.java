package es.ull.iis.ontology.radios.json.schema4simulation.builders;

import es.ull.iis.ontology.radios.Constants;
import es.ull.iis.ontology.radios.json.schema4simulation.Disease;

public class DiseaseBuilder {
	public static Disease getDisease (String diseaseName) {
		Disease disease = new Disease();
		disease.setName(diseaseName);
		disease.setBirthPrevalence(OwlHelper.getDataPropertyValue(diseaseName, Constants.DATAPROPERTY_BIRTH_PREVALENCE));

		disease.setScreeningStrategies(null);
		disease.setClinicalDiagnosisStrategies(null);
		disease.setDevelopments(null);
		disease.setInterventions(null);
		disease.setFollowUpStrategies(null);				
		disease.setTreatmentStrategies(null);

		disease.setScreeningStrategies(ScreeningBuilder.getScreeningStrategies(diseaseName));
		disease.setClinicalDiagnosisStrategies(ClinicalDiagnosisBuilder.getClinicalDiagnosisStrategies(diseaseName));
		disease.setDevelopments(DevelopmentBuilder.getDevelopments(diseaseName));
		disease.setInterventions(InterventionBuilder.getInterventions(diseaseName));
		disease.setFollowUpStrategies(FollowUpBuilder.getFollowUpStrategies(diseaseName));				
		disease.setTreatmentStrategies(TreatmentBuilder.getTreatmentStrategies(diseaseName));

		return disease;
	}
}
