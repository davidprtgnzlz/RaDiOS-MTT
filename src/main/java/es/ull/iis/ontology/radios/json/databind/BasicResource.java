package es.ull.iis.ontology.radios.json.databind;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class BasicResource {
	private String name;
	private String description;

	public BasicResource(String name, String description) {
		this.name = name;
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public static String bindFromList (List<String> source) throws JsonProcessingException {
		return bindFromList(source, null);
	}

	public static String bindFromList (List<String> source, List<BasicResource> initialization) throws JsonProcessingException {
		if (source == null) {
			return null;
		}
		
		List<BasicResource> resources = (initialization != null) ? new ArrayList<>(initialization) : new ArrayList<>();
		for (String item : source) {
			String itemWithoutHastag = item.startsWith("#") ? item.substring(1): item;
			resources.add(new BasicResource(itemWithoutHastag, itemWithoutHastag.replaceAll("_", " ")));
		}		
		
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(resources);
	}
}
