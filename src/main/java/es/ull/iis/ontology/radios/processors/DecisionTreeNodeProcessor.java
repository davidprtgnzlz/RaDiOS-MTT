package es.ull.iis.ontology.radios.processors;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import es.ull.iis.ontology.radios.Constants;
import es.ull.iis.ontology.radios.NodeData;
import es.ull.iis.ontology.radios.TreeNode;
import es.ull.iis.ontology.radios.exceptions.TranspilerException;
import es.ull.iis.ontology.radios.service.DataStoreService;
import es.ull.iis.ontology.radios.utils.CollectionUtils;
import es.ull.iis.ontology.radios.utils.CostsUtils;
import es.ull.iis.ontology.radios.wrappers.TimeRangeManifestationsWrapper;

public class DecisionTreeNodeProcessor extends TreeNodeProcessor {
	public DecisionTreeNodeProcessor(String diseaseName) {
		this.diseaseName = diseaseName;
	}

	/**
	 * @param node
	 * @return
	 */
	private List<TreeNode<NodeData>> getBranchManifestations(TreeNode<NodeData> node) {
		List<TreeNode<NodeData>> result = new ArrayList<>();
		if (!node.getData().getName().startsWith(Constants.CONSTANT_NEGATION)) {
			result.add(node);
		}
		TreeNode<NodeData> n = node.getParent();
		while (Constants.CLASS_MANIFESTATION.equals(n.getData().getType())) {
			if (!n.getData().getName().startsWith(Constants.CONSTANT_NEGATION)) {
				result.add(n);
			}
			n = n.getParent();
		}
		return result;
	}

	/**
	 * @param node
	 * @return
	 */
	private List<TreeNode<NodeData>> getBranchWithoutManifestations(TreeNode<NodeData> node) {
		List<TreeNode<NodeData>> result = new ArrayList<>();
		TreeNode<NodeData> n = node;
		while (n != null) {
			if (!Constants.CLASS_MANIFESTATION.equals(n.getData().getType())) {
				result.add(n);
			}
			n = n.getParent();
		}
		return result;
	}

	/**
	 * @param node
	 * @return
	 */
	private List<TreeNode<NodeData>> getFullBranchToRoot(TreeNode<NodeData> node) {
		List<TreeNode<NodeData>> result = new ArrayList<>();
		result.add(node);
		TreeNode<NodeData> n = node.getParent();
		while (n != null) {
			result.add(n);
			n = n.getParent();
		}
		return result;
	}

	/**
	 * @param manifestations
	 * @return
	 */
	private List<TimeRangeManifestationsWrapper> adjustTimesRangesManifestationsForCosts(List<TreeNode<NodeData>> manifestations) {
		return calculateTimesRangesManifestations(manifestations);
	}

	/**
	 * @param manifestations
	 * @return
	 */
	private List<TimeRangeManifestationsWrapper> adjustTimesRangesManifestationsForUtilities(List<TreeNode<NodeData>> manifestations) {
		List<TimeRangeManifestationsWrapper> timesRangesManifestations = calculateTimesRangesManifestations(manifestations);

		// Calculate the minimum utility per range
		Double utilityGeneralPopulation = Double.valueOf(DataStoreService.getPropertyDatasheet(this.diseaseName, Constants.DATASHEET_UTILITY_GENERAL_POPULATION));
		for (TimeRangeManifestationsWrapper timeRangeManifestation : timesRangesManifestations) {
			Double minimumUtility = utilityGeneralPopulation;
			for (TreeNode<NodeData> manifestation : timeRangeManifestation.getManifestations()) {
				if (manifestation.getData().getProperties().containsKey(Constants.CUSTOM_PROPERTY_UTILITY_VALUE)) {
					Double utilityManifestation = Double.valueOf(manifestation.getData().getProperties().get(Constants.CUSTOM_PROPERTY_UTILITY_VALUE).getValue());
					if (utilityManifestation < minimumUtility) {
						minimumUtility = utilityManifestation;
					}
				}
			}

			timeRangeManifestation.setValue(minimumUtility);
		}

		return timesRangesManifestations;
	}

	/**
	 * @param manifestations
	 * @return
	 */
	private List<TimeRangeManifestationsWrapper> calculateTimesRangesManifestations(List<TreeNode<NodeData>> manifestations) {
		// Calculate the possible ranges
		List<Double> limits = CollectionUtils.asSortedList(getPossibleTimesRanges(manifestations));
		List<TimeRangeManifestationsWrapper> timesRangesManifestations = new ArrayList<TimeRangeManifestationsWrapper>();
		for (int i = 0; i < limits.size() - 1; i++) {
			timesRangesManifestations.add(new TimeRangeManifestationsWrapper(limits.get(i), limits.get(i + 1)));
		}

		// Classify the manifestations in ranges
		for (TreeNode<NodeData> manifestation : manifestations) {
			Double c = Double.valueOf(manifestation.getData().getProperties().get(Constants.DATAPROPERTY_ONSET_AGE).getValue());
			Double d = Double.valueOf(manifestation.getData().getProperties().get(Constants.DATAPROPERTY_END_AGE).getValue());
			for (TimeRangeManifestationsWrapper timeRangeManifestation : timesRangesManifestations) {
				Double a = timeRangeManifestation.getOnSetAge();
				Double b = timeRangeManifestation.getEndAge();
				if (c <= a && b <= d) {
					timeRangeManifestation.getManifestations().add(manifestation);
				}
			}
		}
		return timesRangesManifestations;
	}

	/**
	 * @param manifestations
	 * @return
	 */
	private List<Double> getPossibleTimesRanges(List<TreeNode<NodeData>> manifestations) {
		List<Double> limits = new ArrayList<Double>();

		Double branchLifeExpectancy = calculateBranchLifeExpectancy(manifestations);

		for (TreeNode<NodeData> manifestation : manifestations) {
			Double onSetAge = Double.valueOf(manifestation.getData().getProperties().get(Constants.DATAPROPERTY_ONSET_AGE).getValue());
			Double endAge = Double.valueOf(manifestation.getData().getProperties().get(Constants.DATAPROPERTY_END_AGE).getValue());
			if (!limits.contains(onSetAge)) {
				limits.add(onSetAge);
			}
			if (endAge <= branchLifeExpectancy && !limits.contains(endAge)) {
				limits.add(endAge);
			}
		}
		CollectionUtils.asSortedList(limits);
		return limits;
	}

	/**
	 * @param nodes
	 * @return
	 */
	private Double calculateBranchLifeExpectancy(List<TreeNode<NodeData>> nodes) {
		Double branchLifeExpectancy = Double.valueOf(DataStoreService.getPropertyDatasheet(this.diseaseName, Constants.DATASHEET_NATURAL_DEVELOPMENT_LIFE_EXPECTANCY));
		Double maxMortalityFactor = 0.0;
		for (TreeNode<NodeData> node : nodes) {
			if (node.getData().getProperties().containsKey(Constants.DATAPROPERTY_MORTALITY_FACTOR)) {
				Double nodeMortalityFactor = Double.valueOf(node.getData().getProperties().get(Constants.DATAPROPERTY_MORTALITY_FACTOR).getValue());
				if (nodeMortalityFactor < maxMortalityFactor) {
					maxMortalityFactor = nodeMortalityFactor;
				}
			}
		}
		return branchLifeExpectancy + maxMortalityFactor;
	}

	/**
	 * @param node
	 * @return
	 */
	private Double calculateQalyBranch(TreeNode<NodeData> node) {
		Double naturalDevelopmentLifeExpectancy = Double.valueOf(DataStoreService.getPropertyDatasheet(this.diseaseName, Constants.DATASHEET_NATURAL_DEVELOPMENT_LIFE_EXPECTANCY));
		Double discountRateEffects = Double.valueOf(DataStoreService.getPropertyDatasheet(this.diseaseName, Constants.DATASHEET_DISCOUNT_RATE_EFFECTS));
		Double utilityGeneralPopulation = Double.valueOf(DataStoreService.getPropertyDatasheet(this.diseaseName, Constants.DATASHEET_UTILITY_GENERAL_POPULATION));

		List<TreeNode<NodeData>> branchManifestations = getBranchManifestations(node);
		
		Double result = 0.0;
		if (Constants.CLASS_MANIFESTATION.equals(node.getData().getType())) {
			if (branchManifestations.isEmpty()) {
				result = CostsUtils.calculateDiscount(utilityGeneralPopulation, discountRateEffects, 0.0, naturalDevelopmentLifeExpectancy);
			} else {
				List<TimeRangeManifestationsWrapper> timesRangesManifestations = adjustTimesRangesManifestationsForUtilities(branchManifestations);
				for (TimeRangeManifestationsWrapper timeRangeManifestation : timesRangesManifestations) {
					Double timeRangeManifestationUtilityDiscount = CostsUtils.calculateDiscount(timeRangeManifestation.getValue(), discountRateEffects, timeRangeManifestation.getOnSetAge(),
							timeRangeManifestation.getEndAge());
					result += timeRangeManifestationUtilityDiscount;
				}
				if (timesRangesManifestations != null && !timesRangesManifestations.isEmpty()) {
					// For cases in which the first age range starts above 0 years
					Double minOnSetAge = timesRangesManifestations.get(0).getOnSetAge();
					if (minOnSetAge > 0.0) {
						result += CostsUtils.calculateDiscount(utilityGeneralPopulation, discountRateEffects, 0.0, minOnSetAge);
					}
					
					// For cases where the maximum hasEndAge is prepaid, the rest of the utilities must be taken into account up to the lifeExpectancy of the branch
					Double maxEndAge = timesRangesManifestations.get(timesRangesManifestations.size()-1).getEndAge();
					Double branchLifeExpectancy = calculateBranchLifeExpectancy(branchManifestations); 
					if (maxEndAge < branchLifeExpectancy) {
						result += CostsUtils.calculateDiscount(utilityGeneralPopulation, discountRateEffects, maxEndAge, branchLifeExpectancy);
					}
					
				}
			}
		} else {
			List<TreeNode<NodeData>> fullbranchToRoot = getFullBranchToRoot(node);
			Double minUtilityBranch = utilityGeneralPopulation;
			for (TreeNode<NodeData> n : fullbranchToRoot) {
				if (n.getData().getProperties().containsKey(Constants.CUSTOM_PROPERTY_UTILITY_VALUE)) {
					Double nodeUtility = Double.valueOf(n.getData().getProperties().get(Constants.CUSTOM_PROPERTY_UTILITY_VALUE).getValue());
					if (nodeUtility < minUtilityBranch) {
						minUtilityBranch = nodeUtility;
					}
				}
			}
			Double branchLifeExpectancy = calculateBranchLifeExpectancy(fullbranchToRoot);
			result = CostsUtils.calculateDiscount(minUtilityBranch, discountRateEffects, 0.0, branchLifeExpectancy);
		}
		return result;
	}

	/**
	 * @param node
	 * @return
	 */
	private Double calculateCostBranch(TreeNode<NodeData> node) {
		Double discountRateCosts = Double.valueOf(DataStoreService.getPropertyDatasheet(this.diseaseName, Constants.DATASHEET_DISCOUNT_RATE_COSTS));
		Double naturalDevelopmentLifeExpectancy = Double.valueOf(DataStoreService.getPropertyDatasheet(this.diseaseName, Constants.DATASHEET_NATURAL_DEVELOPMENT_LIFE_EXPECTANCY));

		Double branchLifeExpectancy = naturalDevelopmentLifeExpectancy;
		Double annualCosts = 0.0;
		Double lifeTimeCosts = 0.0;
		Double oneTimeCosts = 0.0;
		Boolean isBranchManifestationsWithoutManifestations = false;
		if (Constants.CLASS_MANIFESTATION.equals(node.getData().getType())) {
			List<TreeNode<NodeData>> branchManifestations = getBranchManifestations(node);
			branchLifeExpectancy = calculateBranchLifeExpectancy(branchManifestations);
			if (!branchManifestations.isEmpty()) {
				// Calculate annual costs for manifestations for ranges
				List<TimeRangeManifestationsWrapper> timesRangesManifestations = adjustTimesRangesManifestationsForCosts(branchManifestations);
				for (TimeRangeManifestationsWrapper timeRangeManifestation : timesRangesManifestations) {
					Double onSetAgeRange = timeRangeManifestation.getOnSetAge();
					Double endAgeRange = timeRangeManifestation.getEndAge();
					for (TreeNode<NodeData> manifestation : timeRangeManifestation.getManifestations()) {
						Double annualCost = Double.valueOf(manifestation.getData().getProperties().get(Constants.CUSTOM_PROPERTY_ANNUAL_COST).getValue());
						Double annualCostDiscount = CostsUtils.calculateDiscount(annualCost, discountRateCosts, onSetAgeRange, endAgeRange); 
						annualCosts += annualCostDiscount;
					}
				}

				// Calculate onetime costs for manifestations for ranges
				for (TreeNode<NodeData> manifestation : branchManifestations) {
					Double onSetAgeRange = Double.valueOf(manifestation.getData().getProperties().get(Constants.DATAPROPERTY_ONSET_AGE).getValue());
					Double oneTimeCost = Double.valueOf(manifestation.getData().getProperties().get(Constants.CUSTOM_PROPERTY_ONETIME_COST).getValue());
					oneTimeCosts += CostsUtils.calculateDiscount2(oneTimeCost, discountRateCosts, onSetAgeRange);
				}

				// Calculate lifetime costs for manifestations for ranges
				for (TreeNode<NodeData> manifestation : branchManifestations) {
					Double lifeTimeCost = Double.valueOf(manifestation.getData().getProperties().get(Constants.CUSTOM_PROPERTY_LIFETIME_COST).getValue());
					lifeTimeCosts += lifeTimeCost;
				}
			} else {
				isBranchManifestationsWithoutManifestations = true;				
			}
		}

		if (!isBranchManifestationsWithoutManifestations) {
			List<TreeNode<NodeData>> branchWithoutManifestations = getBranchWithoutManifestations(node);
			for (TreeNode<NodeData> n : branchWithoutManifestations) {
				if (Constants.DATAPROPERTYVALUE_TEMPORAL_BEHAVIOR_LIFETIME_VALUE.equals(n.getData().getProperties().get(Constants.DATAPROPERTY_TEMPORAL_BEHAVIOR).getValue())) {
					lifeTimeCosts += Double.valueOf(n.getData().getProperties().get(Constants.DATAPROPERTY_COST).getValue());
				} else {
					Double annualCost = Double.valueOf(n.getData().getProperties().get(Constants.DATAPROPERTY_COST).getValue());
					annualCosts += CostsUtils.calculateDiscount(annualCost, discountRateCosts, 0.0, branchLifeExpectancy);
				}
			}
		}

		return annualCosts + lifeTimeCosts + oneTimeCosts;
	}

	/**
	 * @param node
	 * @return
	 */
	private Double calculateProbabilityBracnh(TreeNode<NodeData> node) {
		List<TreeNode<NodeData>> fullBranchToRoot = getFullBranchToRoot(node);
		Double cummulativeProbability = 1.0;
		for (TreeNode<NodeData> n : fullBranchToRoot) {
			Double nodeProbability = Double.valueOf(n.getData().getProperties().get(Constants.DATAPROPERTY_PROBABILITY).getValue());
			if (n.getData().getName().startsWith(Constants.CONSTANT_NEGATION)) {
				nodeProbability = 1.0 - nodeProbability;
			}
			cummulativeProbability *= nodeProbability;
		}
		return cummulativeProbability;
	}

	@Override
	protected void propertyProcessor(TreeNode<NodeData> node) throws TranspilerException {
		if ((node.getChildren() == null || node.getChildren().isEmpty())) {
			// QALYS CALCULATION
			Double resultQalys = calculateQalyBranch(node);
			node.getData().addProperty(Constants.CUSTOM_PROPERTY_UTILITY_VALUE_MINIMUM_WITH_DISCOUNT, String.format(Locale.US, Constants.CONSTANT_DOUBLE_FORMAT_STRING_3DEC, resultQalys),
					Constants.CONSTANT_DOUBLE_TYPE);

			// COSTS CALCULATION
			Double resultCosts = calculateCostBranch(node);
			CostsUtils.addCummulativeCostToNode(node, resultCosts);

			// PROBABILITY CALCULATION
			Double cummulativeProbability = calculateProbabilityBracnh(node);
			node.getData().addProperty(Constants.CUSTOM_PROPERTY_CUMULATIVE_PROBABILITY, String.format(Locale.US, Constants.CONSTANT_DOUBLE_FORMAT_STRING_12DEC, cummulativeProbability),
					Constants.CONSTANT_DOUBLE_TYPE);
		}
	}

	@Override
	protected Double qalysSummatoryProcessor(TreeNode<NodeData> node) throws TranspilerException {
		Double qalyValue = 0.0;
		Double cumulativeProbabilityValue = 0.0;
		if (node.getData().getProperties() != null) {
			if (node.getData().getProperties().containsKey(Constants.CUSTOM_PROPERTY_UTILITY_VALUE_MINIMUM_WITH_DISCOUNT)) {
				qalyValue = Double.valueOf(node.getData().getProperties().get(Constants.CUSTOM_PROPERTY_UTILITY_VALUE_MINIMUM_WITH_DISCOUNT).getValue());
			}
			if (node.getData().getProperties().containsKey(Constants.CUSTOM_PROPERTY_CUMULATIVE_PROBABILITY)) {
				cumulativeProbabilityValue = Double.valueOf(node.getData().getProperties().get(Constants.CUSTOM_PROPERTY_CUMULATIVE_PROBABILITY).getValue());
			}
		}
		return qalyValue * cumulativeProbabilityValue;
	}

	@Override
	protected Double costsSummatoryProcessor(TreeNode<NodeData> node) throws TranspilerException {
		Double costValue = 0.0;
		Double cumulativeProbabilityValue = 0.0;
		if (node.getData().getProperties() != null) {
			if (node.getData().getProperties().containsKey(Constants.CUSTOM_PROPERTY_CUMULATIVE_COST)) {
				costValue = Double.valueOf(node.getData().getProperties().get(Constants.CUSTOM_PROPERTY_CUMULATIVE_COST).getValue());
			}
			if (node.getData().getProperties().containsKey(Constants.CUSTOM_PROPERTY_CUMULATIVE_PROBABILITY)) {
				cumulativeProbabilityValue = Double.valueOf(node.getData().getProperties().get(Constants.CUSTOM_PROPERTY_CUMULATIVE_PROBABILITY).getValue());
			}
		}
		return costValue * cumulativeProbabilityValue;
	}
}
