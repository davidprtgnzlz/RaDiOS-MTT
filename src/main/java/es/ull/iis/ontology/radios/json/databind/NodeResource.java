package es.ull.iis.ontology.radios.json.databind;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NodeResource {
	@JsonProperty("HTMLclass")
	private String htmlClass;
	private Boolean drawLineThrough;

	public NodeResource() {
	}

	public NodeResource(String htmlClass, Boolean drawLineThrough) {
		this.htmlClass = htmlClass;
		this.drawLineThrough = drawLineThrough;
	}

	public String getHtmlClass() {
		return htmlClass;
	}

	public void setHtmlClass(String htmlClass) {
		this.htmlClass = htmlClass;
	}

	public Boolean getDrawLineThrough() {
		return drawLineThrough;
	}

	public void setDrawLineThrough(Boolean drawLineThrough) {
		this.drawLineThrough = drawLineThrough;
	}
}
