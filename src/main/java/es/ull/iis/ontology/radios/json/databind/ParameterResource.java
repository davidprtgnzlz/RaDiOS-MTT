package es.ull.iis.ontology.radios.json.databind;

public class ParameterResource {
	private String name;
	private String value;
	private String distribution;

	public ParameterResource() {
	}

	public ParameterResource(String name, String value, String distribution) {
		super();
		this.name = name;
		this.value = value;
		this.distribution = distribution;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDistribution() {
		return distribution;
	}

	public void setDistribution(String distribution) {
		this.distribution = distribution;
	}
	
	@Override
	public String toString() {
		return getName() + " ::: " + getValue() + " - " + getDistribution();
	}
}
