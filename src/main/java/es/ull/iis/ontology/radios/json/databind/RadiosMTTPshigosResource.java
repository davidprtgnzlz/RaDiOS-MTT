package es.ull.iis.ontology.radios.json.databind;

import java.util.ArrayList;
import java.util.List;

import es.ull.iis.ontology.radios.json.cytoscape.Cytoscape;
import es.ull.iis.ontology.radios.json.linechart.Linecharts;

public class RadiosMTTPshigosResource {
	private Cytoscape cyJson;
	private List<ParameterResource> parameters;
	private RadiosPsighosSimResult simulationResult;
	private Linecharts linecharts;

	public RadiosMTTPshigosResource() {
	}

	public Cytoscape getCyJson() {
		return cyJson;
	}

	public void setCyJson(Cytoscape cyJson) {
		this.cyJson = cyJson;
	}

	public List<ParameterResource> getParameters() {
		if (parameters == null) {
			parameters = new ArrayList<ParameterResource>();
		}
		return parameters;
	}

	public void setParameters(List<ParameterResource> parameters) {
		this.parameters = parameters;
	}

	public RadiosPsighosSimResult getSimulationResult() {
		return simulationResult;
	}

	public void setSimulationResult(RadiosPsighosSimResult simulationResult) {
		this.simulationResult = simulationResult;
	}

	public Linecharts getLinecharts() {
		return linecharts;
	}
	
	public void setLinecharts(Linecharts linecharts) {
		this.linecharts = linecharts;
	}
}
