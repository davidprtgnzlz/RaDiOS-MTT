package es.ull.iis.ontology.radios.json.databind;

import com.fasterxml.jackson.annotation.JsonProperty;

public class StyleResource {
	@JsonProperty("stroke-width")
	private Integer strokeWidth;
	private String stroke;
	@JsonProperty("arrow-start")
	private String arrowStart;
	@JsonProperty("arrow-end")
	private String arrowEnd;

	public StyleResource() {
	}

	public StyleResource(Integer strokeWidth, String stroke, String arrowStart, String arrowEnd) {
		this.strokeWidth = strokeWidth;
		this.stroke = stroke;
		this.arrowStart = arrowStart;
		this.arrowEnd = arrowEnd;
	}

	public Integer getStrokeWidth() {
		return strokeWidth;
	}

	public void setStrokeWidth(Integer strokeWidth) {
		this.strokeWidth = strokeWidth;
	}

	public String getStroke() {
		return stroke;
	}

	public void setStroke(String stroke) {
		this.stroke = stroke;
	}

	public String getArrowStart() {
		return arrowStart;
	}

	public void setArrowStart(String arrowStart) {
		this.arrowStart = arrowStart;
	}

	public String getArrowEnd() {
		return arrowEnd;
	}

	public void setArrowEnd(String arrowEnd) {
		this.arrowEnd = arrowEnd;
	}

}
