package es.ull.iis.ontology.radios.json.databind;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.ull.iis.ontology.radios.Constants;
import es.ull.iis.ontology.radios.NodeData;
import es.ull.iis.ontology.radios.TreeNode;
import es.ull.iis.ontology.radios.service.DataStoreService;
 
public class NodeStructureResource {
	private TextResource text;
	private ConnectorsResource connectors;
	private List<NodeStructureResource> children;
	@JsonProperty("HTMLclass")
	private String htmlClass;


	public NodeStructureResource() {
	}

	public NodeStructureResource(TextResource text, ConnectorsResource connectors, List<NodeStructureResource> children) {
		this.text = text;
		this.connectors = connectors;
		this.children = children;
	}

	public TextResource getText() {
		return text;
	}

	public void setText(TextResource text) {
		this.text = text;
	}

	public ConnectorsResource getConnectors() {
		return connectors;
	}

	public void setConnectors(ConnectorsResource connectors) {
		this.connectors = connectors;
	}

	public List<NodeStructureResource> getChildren() {
		if (this.children == null) {
			this.children = new ArrayList<>();
		}
		return this.children;
	}

	public void setChildren(List<NodeStructureResource> children) {
		this.children = children;
	}

	public String getHtmlClass() {
		return htmlClass;
	}

	public void setHtmlClass(String htmlClass) {
		this.htmlClass = htmlClass;
	}

	public static NodeStructureResource emptyResource () {
		ConnectorsResource connectors = new ConnectorsResource(null, new StyleResource(null, null, "none-wide-long", null));
		return new NodeStructureResource(new TextResource ("\b", "\b", ""), connectors, new ArrayList<>());
	}

	public static NodeStructureResource toResource (TreeNode<NodeData> node) {
		if (node == null) {
			return null;
		}
		
		StringBuilder nodeDesc = new StringBuilder("");
		StringBuilder nodeTitle = new StringBuilder("");
		if (node.getData().getProperties() != null) {
			for (String property : node.getData().getProperties().keySet()) {
				nodeTitle.append(String.format("%s=%s\n", node.getData().getProperties().get(property).getName().replace("#", ""), node.getData().getProperties().get(property).getValue()));
				if (property.equals(Constants.CUSTOM_PROPERTY_CUMULATIVE_PROBABILITY)) {
					nodeDesc.append(String.format(" P=%s", node.getData().getProperties().get(Constants.CUSTOM_PROPERTY_CUMULATIVE_PROBABILITY).getValue()));					
				}
				if (node.getChildren() == null || node.getChildren().isEmpty()) {
					if (property.equals(Constants.CUSTOM_PROPERTY_CUMULATIVE_COST)) {
						nodeDesc.append(String.format(" C=%s", node.getData().getProperties().get(Constants.CUSTOM_PROPERTY_CUMULATIVE_COST).getValue()));					
					}
					if (property.equals(Constants.CUSTOM_PROPERTY_UTILITY_VALUE_MINIMUM_WITH_DISCOUNT)) {
						nodeDesc.append(String.format(" QALY=%s", node.getData().getProperties().get(Constants.CUSTOM_PROPERTY_UTILITY_VALUE_MINIMUM_WITH_DISCOUNT).getValue()));					
					}
				}
			}
		}
		nodeDesc.append("");
		
		ConnectorsResource connectors = null;
		if (node.getParent() == null) {
			connectors = new ConnectorsResource(null, new StyleResource(null, null, null, "square-wide-long"));
		} else if (node.getChildren() == null || node.getChildren().isEmpty()) {
			connectors = new ConnectorsResource(null, new StyleResource(null, null, "invblock-wide-long", null));
		} else {
			connectors = new ConnectorsResource(null, new StyleResource(null, null, "oval-wide-long", null));
		}

		NodeStructureResource nodeStructure = new NodeStructureResource();
		nodeStructure.setConnectors(connectors);
		
		if (node.getChildren() == null || node.getChildren().isEmpty()) {
			nodeStructure.setHtmlClass("node-leaf");
		}
		
		if (Constants.CLASS_INTERVENTION.equals(node.getData().getType())) {
			nodeStructure.setHtmlClass("node-intervention");
			String interventionSummaryCosts = DataStoreService.getPropertyDatasheet(node.getParent().getData().getName(), node.getData().getName(), Constants.DATASHEET_INTERVENTION_SUMMARY_COSTS);
			nodeDesc.append(String.format(" SC=%s", interventionSummaryCosts));
			String interventionSummaryQalys = DataStoreService.getPropertyDatasheet(node.getParent().getData().getName(), node.getData().getName(), Constants.DATASHEET_INTERVENTION_SUMMARY_QALYS);
			nodeDesc.append(String.format(" SQ=%s", interventionSummaryQalys));
		}
		
		if (Constants.CLASS_DISEASE.equals(node.getData().getType())) {
			nodeStructure.setHtmlClass("node-disease");
		}

		TextResource text = new TextResource (node.getData().getName().replace("#", ""), nodeDesc.toString(), nodeTitle.toString()) ;
		nodeStructure.setText(text);

		return nodeStructure;		
	}
}
