package es.ull.iis.ontology.radios.wrappers;

import java.util.ArrayList;
import java.util.List;

import es.ull.iis.ontology.radios.NodeData;
import es.ull.iis.ontology.radios.TreeNode;

public class TimeRangeManifestationsWrapper {
	private Double onSetAge;
	private Double endAge;
	private List<TreeNode<NodeData>> manifestations;
	private Double value;

	public TimeRangeManifestationsWrapper(Double onSetAge, Double endAge) {
		this.onSetAge = onSetAge;
		this.endAge = endAge;
	}

	public Double getOnSetAge() {
		return onSetAge;
	}

	public void setOnSetAge(Double onSetAge) {
		this.onSetAge = onSetAge;
	}

	public Double getEndAge() {
		return endAge;
	}

	public void setEndAge(Double endAge) {
		this.endAge = endAge;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public List<TreeNode<NodeData>> getManifestations() {
		if (this.manifestations == null) {
			this.manifestations = new ArrayList<TreeNode<NodeData>>();
		}
		return this.manifestations;
	}

	public void setManifestations(List<TreeNode<NodeData>> manifestations) {
		this.manifestations = manifestations;
	}

	@Override
	public String toString() {
		return String.format("%f - %f: %s", onSetAge, endAge, manifestations);
	}
}