package es.ull.iis.ontology.radios.json.databind;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class TreantTreeStructureResource {
	private ChartResource chart;
	private NodeStructureResource nodeStructure;

	public ChartResource getChart() {
		return chart;
	}

	public void setChart(ChartResource chart) {
		this.chart = chart;
	}

	public NodeStructureResource getNodeStructure() {
		return nodeStructure;
	}

	public void setNodeStructure(NodeStructureResource nodeStructure) {
		this.nodeStructure = nodeStructure;
	}

	public static void main(String[] args) throws JsonProcessingException {
		TreantTreeStructureResource treeStructure = new TreantTreeStructureResource();

		treeStructure.setChart(new ChartResource("#DecisionTree", 5, 20, 20, "WEST"));
		treeStructure.getChart().setNode(new NodeResource("decisiontree-draw", true));
		treeStructure.getChart().setConnectors(new ConnectorsResource("straight", new StyleResource(2, "#777", null, null)));

		NodeStructureResource nodeStructure = new NodeStructureResource();
		nodeStructure.setText(new TextResource("PBD", "Profound DB", ""));
		nodeStructure.setConnectors(new ConnectorsResource(null, new StyleResource(null, null, null, "square-wide-long")));
		
		NodeStructureResource nodeStructureChild = new NodeStructureResource();
		nodeStructureChild.setText(new TextResource("Screening", "p=0.967", ""));
		nodeStructureChild.setConnectors(new ConnectorsResource(null, new StyleResource(null, null, "oval-wide-long", null)));
		
		nodeStructure.getChildren().add(nodeStructureChild);
		
		treeStructure.setNodeStructure(nodeStructure);		
		
		ObjectMapper mapper = new ObjectMapper()
				.enable(SerializationFeature.INDENT_OUTPUT)
				.setSerializationInclusion(Include.NON_NULL)
				.setSerializationInclusion(Include.NON_EMPTY);
		System.out.println(mapper.writeValueAsString(treeStructure));
	}
}
