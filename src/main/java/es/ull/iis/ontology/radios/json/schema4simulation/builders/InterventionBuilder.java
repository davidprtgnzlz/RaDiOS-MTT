package es.ull.iis.ontology.radios.json.schema4simulation.builders;

import java.util.ArrayList;
import java.util.List;

import es.ull.iis.ontology.radios.Constants;
import es.ull.iis.ontology.radios.json.schema4simulation.DevelopmentModification;
import es.ull.iis.ontology.radios.json.schema4simulation.Intervention;
import es.ull.iis.ontology.radios.json.schema4simulation.ManifestationModification;
import es.ull.iis.ontology.radios.utils.CollectionUtils;

public class InterventionBuilder {
	public static List<Intervention> getInterventions(String diseaseName) {
		List<Intervention> result = new ArrayList<>();
		List<String> interventions = OwlHelper.getChildsByClassName(diseaseName, Constants.CLASS_INTERVENTION);
		for (String interventionName: interventions) {
			Intervention intervention = new Intervention();
			intervention.setName(interventionName);
			intervention.setKind(OwlHelper.getDataPropertyValue(interventionName, Constants.DATAPROPERTY_KIND_INTERVENTION));
			intervention.setScreeningStrategies(ScreeningBuilder.getScreeningStrategies(interventionName));
			intervention.setClinicalDiagnosisStrategies(ClinicalDiagnosisBuilder.getClinicalDiagnosisStrategies(interventionName));
			intervention.setDevelopmentModifications(getDevelopmentModifications(interventionName));
			intervention.setManifestationModifications(getManifestationModifications(interventionName));
			intervention.setTreatmentStrategies(TreatmentBuilder.getTreatmentStrategies(interventionName));
			intervention.setFollowUpStrategies(FollowUpBuilder.getFollowUpStrategies(interventionName));			
			result.add(intervention);
		}
		return !result.isEmpty() ? result : null;
	}
	
	public static List<ManifestationModification> getManifestationModifications(String interventionName) {
		List<ManifestationModification> result = new ArrayList<>();
		List<String> manifestationModifications = OwlHelper.getChildsByClassName(interventionName, Constants.CLASS_MANIFESTATIONMODIFICATION);
		for (String manifestationModificationName: manifestationModifications) {
			result.add(getManifestationModification(manifestationModificationName));
		}
		return !CollectionUtils.isEmpty(result) ? result : null;
	}

	public static ManifestationModification getManifestationModification(String manifestationModificationName) {
		ManifestationModification manifestationModification = new ManifestationModification();

		manifestationModification.setName(manifestationModificationName);
		manifestationModification.setFrequencyModification(OwlHelper.getDataPropertyValue(manifestationModificationName, Constants.DATAPROPERTY_FREQUENCY_MODIFICATION));
		manifestationModification.setMortalityFactorModification(OwlHelper.getDataPropertyValue(manifestationModificationName, Constants.DATAPROPERTY_MORTALITY_FACTOR_MODIFICATION));
		manifestationModification.setProbabilityModification(OwlHelper.getDataPropertyValue(manifestationModificationName, Constants.DATAPROPERTY_PROBABILITY_MODIFICATION));
		manifestationModification.setProbabilityOfDiagnosisModification(OwlHelper.getDataPropertyValue(manifestationModificationName, Constants.DATAPROPERTY_PROBABILITYOFLEADINGTODIAGNOSISMODIFICATION));
		manifestationModification.setRelativeRiskModification(OwlHelper.getDataPropertyValue(manifestationModificationName, Constants.DATAPROPERTY_RELATIVE_RISK_MODIFICATION));

		manifestationModification.setManifestations(getManifestations(manifestationModificationName));
		return manifestationModification;
	}

	public static List<String> getManifestations(String manifestationModificationName) {
		List<String> result = new ArrayList<>();
		List<String> manifestations = OwlHelper.getObjectPropertiesByName(manifestationModificationName, Constants.OBJECTPROPERTY_MANIFESTATION);
		if (!CollectionUtils.isEmpty(manifestations)) {
			result.addAll(manifestations);
		}
		return !CollectionUtils.isEmpty(result) ? result : null;
	}
	
	public static List<DevelopmentModification> getDevelopmentModifications(String interventionName) {
		List<DevelopmentModification> result = new ArrayList<>();
		List<String> developmentModifications = OwlHelper.getChildsByClassName(interventionName, Constants.CLASS_DEVELOPMENTMODIFICACION);
		for (String developmentModificationName: developmentModifications) {
			result.add(getDevelopmentModification(developmentModificationName));
		}
		return !CollectionUtils.isEmpty(result) ? result : null;
	}

	public static DevelopmentModification getDevelopmentModification(String developmentModificationName) {
		DevelopmentModification developmentModification = new DevelopmentModification();

		developmentModification.setName(developmentModificationName);
		developmentModification.setLifeExpectancyModification(OwlHelper.getDataPropertyValue(developmentModificationName, Constants.DATAPROPERTY_LIFE_EXPECTANCY));
		developmentModification.setModificationForAllParams(OwlHelper.getDataPropertyValue(developmentModificationName, Constants.DATAPROPERTY_MODIFICATIONFORALLPARAMS));
		
		developmentModification.setDevelopments(getDevelopments(developmentModificationName));
		return developmentModification;
	}

	public static List<String> getDevelopments(String developmentModificationName) {
		List<String> result = new ArrayList<>();
		List<String> developments = OwlHelper.getObjectPropertiesByName(developmentModificationName, Constants.OBJECTPROPERTY_DEVELOPMENT);
		if (!CollectionUtils.isEmpty(developments)) {
			result.addAll(developments);
		}
		return !CollectionUtils.isEmpty(result) ? result : null;
	}
	
}
