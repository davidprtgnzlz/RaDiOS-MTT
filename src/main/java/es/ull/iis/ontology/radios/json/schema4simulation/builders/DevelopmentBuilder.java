package es.ull.iis.ontology.radios.json.schema4simulation.builders;

import java.util.ArrayList;
import java.util.List;

import es.ull.iis.ontology.radios.Constants;
import es.ull.iis.ontology.radios.json.schema4simulation.Development;
import es.ull.iis.ontology.radios.json.schema4simulation.Manifestation;
import es.ull.iis.ontology.radios.json.schema4simulation.PrecedingManifestation;
import es.ull.iis.ontology.radios.utils.CollectionUtils;

public class DevelopmentBuilder {
	public static List<Development> getDevelopments(String diseaseName) {
		List<Development> result = new ArrayList<>();
		List<String> developments = OwlHelper.getChildsByClassName(diseaseName, Constants.CLASS_DEVELOPMENT);
		for (String developmentName: developments) {
			result.add(getDevelopment(developmentName));
		}
		return result;
	}

	public static Development getDevelopment(String developmentName) {
		Development development = new Development();

		development.setName(developmentName);
		development.setKind(OwlHelper.getDataPropertyValue(developmentName, Constants.DATAPROPERTY_KIND_DEVELOPMENT));
		development.setLifeExpectancy(OwlHelper.getDataPropertyValue(developmentName, Constants.DATAPROPERTY_LIFE_EXPECTANCY));
		development.setStage(OwlHelper.getDataPropertyValue(developmentName, Constants.DATAPROPERTY_STAGE));
		
		development.setManifestations(getManifestations(developmentName));
		return development;
	}

	public static List<Manifestation> getManifestations(String developmentName) {
		List<Manifestation> result = new ArrayList<>();
		List<String> manifestations = OwlHelper.getChildsByClassName(developmentName, Constants.CLASS_MANIFESTATION);
		for (String manifestationName: manifestations) {
			result.add(getManifestation(manifestationName));
		}
		return result;
	}

	public static Manifestation getManifestation(String manifestationName) {
		Manifestation manifestation = new Manifestation();

		manifestation.setName(manifestationName);
		manifestation.setDuration(OwlHelper.getDataPropertyValue(manifestationName, Constants.DATAPROPERTY_DURATION));
		manifestation.setEndAge(OwlHelper.getDataPropertyValue(manifestationName, Constants.DATAPROPERTY_END_AGE));
		manifestation.setFrequency(OwlHelper.getDataPropertyValue(manifestationName, Constants.DATAPROPERTY_FREQUENCY));
		manifestation.setKind(OwlHelper.getDataPropertyValue(manifestationName, Constants.DATAPROPERTY_KIND_MANIFESTATION));
		
		manifestation.setMortalityFactor(CrossBuilder.recalculatePropabilityField(manifestationName, Constants.DATAPROPERTY_MORTALITY_FACTOR, Constants.DATAPROPERTY_MORTALITY_FACTOR_DISTRIBUTION));
		manifestation.setProbability(CrossBuilder.recalculatePropabilityField(manifestationName, Constants.DATAPROPERTY_PROBABILITY, Constants.DATAPROPERTY_PROBABILITY_DISTRIBUTION));

		manifestation.setOnSetAge(OwlHelper.getDataPropertyValue(manifestationName, Constants.DATAPROPERTY_ONSET_AGE));
		manifestation.setProbabilityOfDiagnosis(OwlHelper.getDataPropertyValue(manifestationName, Constants.DATAPROPERTY_PROBABILITYOFLEADINGTODIAGNOSIS));
		manifestation.setRelativeRisk(OwlHelper.getDataPropertyValue(manifestationName, Constants.DATAPROPERTY_RELATIVE_RISK));

		manifestation.setPrecedingManifestations(getPrecedingManifestations(manifestationName));
		manifestation.setCosts(CrossBuilder.getCosts(manifestationName));		
		manifestation.setUtilities(CrossBuilder.getUtilities(manifestationName));		
		manifestation.setTreatmentStrategies(TreatmentBuilder.getTreatmentStrategies(manifestationName));		
		manifestation.setFollowUpStrategies(FollowUpBuilder.getFollowUpStrategies(manifestationName));
		return manifestation;
	}

	public static List<PrecedingManifestation> getPrecedingManifestations(String manifestationName) {
		List<PrecedingManifestation> result = new ArrayList<>();
		List<String> oPrecedingManifestations = OwlHelper.getObjectPropertiesByName(manifestationName, Constants.OBJECTPROPERTY_MANIFESTATION_PRECEDINGMANIFESTATIONS);
		if (!CollectionUtils.isEmpty(oPrecedingManifestations)) {
			for (String oPrecedingManifestationsName: oPrecedingManifestations) {
				List<String> precedingManifestations = OwlHelper.getObjectPropertiesByName(oPrecedingManifestationsName, Constants.OBJECTPROPERTY_PRECEDINGMANIFESTATIONS_PRECEDINGMANIFESTATION);
				for (String precedingManifestationName: precedingManifestations) {
					String probability = CrossBuilder.recalculatePropabilityField(oPrecedingManifestationsName, Constants.DATAPROPERTY_PROBABILITY, Constants.DATAPROPERTY_PROBABILITY_DISTRIBUTION);
					result.add(new PrecedingManifestation(precedingManifestationName, probability, OwlHelper.getDataPropertyValue(oPrecedingManifestationsName, Constants.DATAPROPERTY_REPLACEPREVIOUS)));
				}
			}
		}
		return !CollectionUtils.isEmpty(result) ? result : null;
	}
}
