package es.ull.iis.ontology.radios.restapi;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.xsd.owl2.Ontology;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import es.ull.iis.ontology.radios.Constants;
import es.ull.iis.ontology.radios.PropertyData;
import es.ull.iis.ontology.radios.exceptions.TranspilerException;
import es.ull.iis.ontology.radios.json.databind.BasicResource;
import es.ull.iis.ontology.radios.json.databind.DiseaseResource;
import es.ull.iis.ontology.radios.json.databind.InputParametersDecisionTree;
import es.ull.iis.ontology.radios.json.databind.InputParametersDesSimulation;
import es.ull.iis.ontology.radios.json.databind.InterventionResource;
import es.ull.iis.ontology.radios.json.databind.StrategyResource;
import es.ull.iis.ontology.radios.service.DataStoreService;
import es.ull.iis.ontology.radios.service.RadiosService;
import es.ull.iis.ontology.radios.utils.OntologyUtils;
import es.ull.iis.simulation.hta.radios.exceptions.TransformException;

@Path("/api/v2")
public class RadiosRestApi {
	private static Log LOGGER = LogFactory.getLog(RadiosRestApi.class);
	
	@Context private ServletContext servletContext;
	private static Ontology raDiOSInstance; 
	
	private Ontology getRaDiOSInstance (ServletContext servletContext) throws IOException, JAXBException {
		if (raDiOSInstance == null) {
			raDiOSInstance = getRaDiOSInstance(servletContext, 0);
		}
		return raDiOSInstance;
	}
	private Ontology getRaDiOSInstance (ServletContext servletContext, int idFile) throws IOException, JAXBException {
		String basePath = "/WEB-INF/classes/owl/";
		String path = null;
		if (idFile == 100) {			
			path = basePath + "radios_PBD.owl";
		} else if (idFile == 101) {
			path = basePath + "radios_SCD.owl";
		} else if (idFile == 1) {
			path = basePath + "radios-test_disease1.owl";
		} else if (idFile == 2) {
			path = basePath + "radios-test_disease2.owl";
		} else if (idFile == 3) {
			path = basePath + "radios-test_disease3.owl";
		} else if (idFile == 4) {			
			path = basePath + "radios-test_disease4.owl";
		}
			
		try (InputStream radiosOWLFile = servletContext.getResourceAsStream(path);) {
			raDiOSInstance = OntologyUtils.loadOntology(radiosOWLFile);
		}
		return raDiOSInstance;
	}
	
	private Response createErrorJsonResponse(String msg) {
		String responseBody = String.format("{ \"error\": \"%s\" }", msg);
		return Response.status(200).type(MediaType.APPLICATION_JSON_TYPE).entity(responseBody).build();
	}
	
	@GET
	@Path("/radios-ontology")
	@Produces(MediaType.APPLICATION_XML)
	public Response radiosOntology() {
		try {
			return Response.status(200).type(MediaType.APPLICATION_XML).entity(OntologyUtils.getRadiosAsString(getRaDiOSInstance(servletContext))).build();
		} catch (IOException | JAXBException e) {
			return createErrorJsonResponse("The ontology xml file was not found or you are corrupt !!!");
		}
	}

	@GET
	@Path("/{idFile}/diseases")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDiseases(@PathParam("idFile") String idFile) {
		if (idFile == null || !NumberUtils.isDigits(idFile))	{
			return createErrorJsonResponse("You must specify the parameter owl file (previous select).");
		}

		String responseBody = "";
		try {
			Map<String, String> instancesToClazz = new HashMap<String, String>();
			Map<String, List<String>> instancesByClazz = new HashMap<String, List<String>>();
			DataStoreService.eTLClassIndividuals(getRaDiOSInstance(servletContext, Integer.parseInt(idFile)), instancesByClazz, instancesToClazz, Constants.CLASS_DISEASE);
		
			responseBody = DiseaseResource.bindFromList(instancesByClazz.get(Constants.CLASS_DISEASE));
			return Response.status(200).type(MediaType.APPLICATION_JSON_TYPE).entity(responseBody).build();
		} catch (IOException | JAXBException e) {
			return createErrorJsonResponse("An error occurred while trying to retrieve the list of diseases stored in RaDiOS !!");
		} 
	}

	@GET
	@Path("/{disease}/interventions")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getIntervetionsFromDisease(@PathParam("disease") String disease) {
		String responseBody = "";
		try {
			Map<String, String> instancesToClazz = new HashMap<String, String>();
			Map<String, List<String>> instancesByClazz = new HashMap<String, List<String>>();

			DataStoreService.eTLClassIndividuals(getRaDiOSInstance(servletContext), instancesByClazz, instancesToClazz, Constants.CLASS_DISEASE);

			List<String> interventions = instancesByClazz.get(Constants.CLASS_INTERVENTION);
			BasicResource doNothing = new BasicResource(Constants.CONSTANT_DO_NOTHING, "Follow the natural course of the disease"); 
			responseBody = InterventionResource.bindFromList(interventions == null ? new ArrayList<>() : interventions, Arrays.asList(doNothing));
			return Response.status(200).type(MediaType.APPLICATION_JSON_TYPE).entity(responseBody).build();
		} catch (IOException | JAXBException e) {
			return createErrorJsonResponse(String.format("An error occurred while trying to retrieve the list of interventions stored in RaDiOS from [%s] !!", disease));
		} 
	}

	@GET
	@Path("/{disease}/clinicalDiagnosisStrategy")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getClinicalDiagnosisStrategiesFromDisease(@PathParam("disease") String disease) {
		String responseBody = "";
		try {
			Map<String, String> instancesToClazz = new HashMap<String, String>();
			Map<String, List<String>> instancesByClazz = new HashMap<String, List<String>>();
			DataStoreService.eTLClassIndividuals(getRaDiOSInstance(servletContext), instancesByClazz, instancesToClazz, Constants.CLASS_DISEASE);
			
			responseBody = StrategyResource.bindFromList(instancesByClazz.get(Constants.CLASS_CLINICALDIAGNOSISSTRATEGY));
			return Response.status(200).type(MediaType.APPLICATION_JSON_TYPE).entity(responseBody).build();
		} catch (IOException | JAXBException e) {			
			return createErrorJsonResponse(String.format("An error occurred while trying to retrieve the list of strategies stored in RaDiOS from [%s] !!", disease));
		}
	}

	@GET
	@Path("/decision-tree/{disease}/{interventionA}/{interventionB}/{clinicalDiagnosisStrategy}/asText")
	@Produces(MediaType.TEXT_HTML)
	public Response decisionTree(@PathParam("disease") String disease, @PathParam("interventionA") String interventionA, @PathParam("interventionB") String interventionB,
			@PathParam("clinicalDiagnosisStrategy") String clinicalDiagnosisStrategy) {
		if (clinicalDiagnosisStrategy == null || clinicalDiagnosisStrategy.isEmpty() || interventionA == null || interventionA.isEmpty() || interventionB == null || interventionB.isEmpty()
				|| disease == null || disease.isEmpty()) {
			return createErrorJsonResponse("You must specify the parameters: clinicalDiagnosisStrategy, intervetionA, interventionB and disease");
		}

		try {
			String plainDecisionTree = RadiosService.buildPlainDecisionTree(getRaDiOSInstance(servletContext), disease, interventionA, interventionB, clinicalDiagnosisStrategy);
			String title = String.format("Decision Tree generated for Disease: [%s]", disease);		
			return Response.status(200).type(MediaType.TEXT_HTML).entity(String.format("<html><head><title>%s</title></head><body><pre>%s</pre></body></html>", title, plainDecisionTree)).build();
		} catch (TranspilerException | IOException | JAXBException e) {
			return createErrorJsonResponse(String.format("An error occurred when trying to build the decision tree from the input parameters [%s]", e.getMessage()));
		}
	}

	@POST
	@Path("/decision-tree/asJson")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response decisionTreeAsJson(InputParametersDecisionTree inputParameters) {
		LOGGER.info("Invoking api method decisionTreeAsJson ...");
		if (inputParameters == null 
				|| inputParameters.getClinicalDiagnosisStrategy() == null || inputParameters.getClinicalDiagnosisStrategy().isEmpty() 
				|| inputParameters.getInterventions() == null || inputParameters.getInterventions().isEmpty()
				|| inputParameters.getDisease() == null || inputParameters.getDisease().isEmpty() 
				|| inputParameters.getFramework() == null || inputParameters.getFramework().isEmpty())	{
			return createErrorJsonResponse("You must specify the parameters: clinicalDiagnosisStrategy, intervetionA, interventionB and disease.");
		}
		
		String responseBody = null;
		int responseCode = 200;
		if ("treant".equals(inputParameters.getFramework().toLowerCase())) {
			try {
				ObjectMapper mapper = new ObjectMapper()
						.enable(SerializationFeature.INDENT_OUTPUT)
						.setSerializationInclusion(Include.NON_NULL)
						.setSerializationInclusion(Include.NON_EMPTY)
						.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
				responseBody = mapper.writeValueAsString(RadiosService.buildTreantDecisionTree(getRaDiOSInstance(servletContext), inputParameters));
				
			} catch (TranspilerException | IOException | JAXBException e) {
				responseBody = String.format("{ \"error\": \"%s\" }", e.getMessage());
				responseCode = 200;
				LOGGER.error(e);
			} catch (NumberFormatException e) {
				responseBody = String.format("{ \"error\": \"%s\" }", "The definition of the disease contains probabilities not treatable by the generation of the decision tree algorithm.");
				responseCode = 200;
				LOGGER.error(e);
			}
		} else {
			responseBody = "{ \"error\": \"Framework unknow !!\" }";
			responseCode = 200;
		}

		return Response.status(responseCode).type(MediaType.APPLICATION_JSON_TYPE).entity(responseBody).build();
	}
	
	@GET
	@Path("/{disease}/datasheet")
	@Produces(MediaType.TEXT_PLAIN)
	public Response getDiseaseDataProperties(@PathParam("disease") String disease) {
		if (disease == null) {
			return Response.status(200).type(MediaType.TEXT_PLAIN).entity("¡¡ Data not found !!").build();
		}
		
		StringBuilder sb = new StringBuilder();
		Map<String, List<PropertyData>> datasheet = DataStoreService.getDiseasesDatasheetInstance().get(Constants.CONSTANT_HASHTAG + disease);
		if (datasheet != null) {
			for (String key : datasheet.keySet()) {
				List<PropertyData> properties = datasheet.get(key); 
				for (PropertyData propertyData : properties) {
					String propertyName = propertyData.getName();
					if (key.equals(propertyName)) {
						sb.append(String.format("%s = %s\n", (key.startsWith(Constants.CONSTANT_HASHTAG) ? key.substring(1) : key), propertyData.getValue()));
					} else {
						sb.append(String.format("%s (%s) = %s\n", (key.startsWith(Constants.CONSTANT_HASHTAG) ? key.substring(1) : key), propertyName, propertyData.getValue()));
					}
				}
			}
			return Response.status(200).type(MediaType.TEXT_PLAIN).entity(sb.toString()).build();
		} else {
			return Response.status(200).type(MediaType.TEXT_PLAIN).entity("Data not found !!").build();
		}
	}		
	
	@POST
	@Path("/des-simulation")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response desSimulation(InputParametersDesSimulation inputParameters) {
		if (inputParameters == null 
				|| inputParameters.getInterventions() == null || inputParameters.getInterventions().isEmpty()
				|| inputParameters.getDisease() == null || inputParameters.getDisease().isEmpty())	{
			return createErrorJsonResponse("You must specify the parameters: intervetionA, interventionB and disease.");
		}
		
		String responseBody = null;
		int responseCode = 200;
		try {
			ObjectMapper mapper = new ObjectMapper()
					.enable(SerializationFeature.INDENT_OUTPUT)
					.setSerializationInclusion(Include.NON_NULL)
					.setSerializationInclusion(Include.NON_EMPTY)
					.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
			
			responseBody = mapper.writeValueAsString(RadiosService.desSimulation(getRaDiOSInstance(servletContext), normalizeInputParameters(inputParameters)));			
		} catch (TransformException| TranspilerException | IOException | JAXBException e) {
			responseBody = String.format("{ \"error\": \"%s\" }", e.getMessage());
			responseCode = 200;
			LOGGER.error(e.getMessage(), e);
		} catch (NumberFormatException e) {
			responseBody = String.format("{ \"error\": \"%s\" }", "The definition of the disease contains probabilities not treatable by the generation of the decision tree algorithm.");
			responseCode = 200;
			LOGGER.error(e.getMessage(), e);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}

		return Response.status(responseCode).type(MediaType.APPLICATION_JSON_TYPE).entity(responseBody).build();
	}
	
	/**
	 * @param inputParameters
	 * @return
	 */
	private InputParametersDesSimulation normalizeInputParameters(InputParametersDesSimulation inputParameters) {
		if (!inputParameters.getDisease().startsWith("#")) {
			inputParameters.setDisease("#" + inputParameters.getDisease());
		}
		
		List<String> interventions = new ArrayList<>();
		for (String intervention : inputParameters.getInterventions()) {
			if (!Constants.CONSTANT_DO_NOTHING.equals(intervention)) {
				interventions.add("#" + intervention);
			} else {
				interventions.add(intervention);
			}
		}	
		inputParameters.setInterventions(interventions);
		
		if (inputParameters.getAllPatientsAffected() == null) {
			inputParameters.setAllPatientsAffected(false);				
		}
		
		if (inputParameters.getUtilityGeneralPopulation() == null) {
			inputParameters.setUtilityGeneralPopulation(1.0);				
		}
		
		return inputParameters;
	}
	
}
