package es.ull.iis.ontology.radios.processors;

import java.util.List;

import es.ull.iis.ontology.radios.NodeData;
import es.ull.iis.ontology.radios.TreeNode;
import es.ull.iis.ontology.radios.exceptions.TranspilerException;
import es.ull.iis.ontology.radios.utils.TreeUtils;

public abstract class TreeNodeProcessor {
	protected String diseaseName = null;

	public void processProperties(TreeNode<NodeData> node) throws TranspilerException {
		if (node == null) {
			return;
		}
		propertyProcessor(node);
		for (TreeNode<NodeData> n : node.getChildren()) {
			processProperties(n);
		}
	}

	public Double processQalys(TreeNode<NodeData> node) throws TranspilerException {
		List<TreeNode<NodeData>> leafs = TreeUtils.getTreeNodeLeafs(node);
		Double cumulativeValue = 0.0;
		for (TreeNode<NodeData> n : leafs) {
			cumulativeValue += qalysSummatoryProcessor(n); 
		}
		return cumulativeValue;
	}

	public Double processCosts(TreeNode<NodeData> node) throws TranspilerException {
		List<TreeNode<NodeData>> leafs = TreeUtils.getTreeNodeLeafs(node);
		Double cumulativeValue = 0.0;
		for (TreeNode<NodeData> n : leafs) {
			cumulativeValue += costsSummatoryProcessor(n);
		}
		return cumulativeValue;
	}

	protected abstract void propertyProcessor(TreeNode<NodeData> node) throws TranspilerException;
	protected abstract Double qalysSummatoryProcessor(TreeNode<NodeData> node) throws TranspilerException;
	protected abstract Double costsSummatoryProcessor(TreeNode<NodeData> node) throws TranspilerException;
}
