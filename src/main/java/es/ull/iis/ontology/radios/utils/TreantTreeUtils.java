package es.ull.iis.ontology.radios.utils;

import java.io.IOException;

import es.ull.iis.ontology.radios.NodeData;
import es.ull.iis.ontology.radios.TreeNode;
import es.ull.iis.ontology.radios.exceptions.TranspilerException;
import es.ull.iis.ontology.radios.json.databind.NodeStructureResource;
import es.ull.iis.ontology.radios.wrappers.IntegerWrapper;

public interface TreantTreeUtils {

	public static void inorden(TreeNode<NodeData> node, NodeStructureResource treantNode, int level, IntegerWrapper maxLevel) throws TranspilerException, IOException {
		if (node == null) {
			return;
		}

		if (maxLevel.value == null || level > maxLevel.value) {
			maxLevel.value = new Integer(level);
		}
		if (!node.getChildren().isEmpty()) {
			for (TreeNode<NodeData> n : node.getChildren()) {
				NodeStructureResource nTreant = NodeStructureResource.toResource(n);
				treantNode.getChildren().add(nTreant);
				inorden(n, nTreant, level + 1, maxLevel);
			}
		}
	}

	public static void completeChildsToMaxLevelTree(NodeStructureResource node, Integer level, Integer maxLevel) {
		if (node == null) {
			return;
		}

		for (NodeStructureResource n : node.getChildren()) {
			completeChildsToMaxLevelTree(n, level + 1, maxLevel);
		}
		if (node.getChildren().isEmpty()) {
			int numChildsPlus = maxLevel.intValue() - level.intValue();
			NodeStructureResource tmp = node;
			for (int i = 0; i < numChildsPlus; i++) {
				NodeStructureResource child = NodeStructureResource.emptyResource();

				if (i == 0) {
					tmp.getConnectors().getStyle().setArrowStart(null);
				} 
				if (i == numChildsPlus - 1) {
					child.getConnectors().getStyle().setArrowStart("invblock-wide-long");
				}

				tmp.getChildren().add(child);
				tmp = child;
			}
		}

	}
}
