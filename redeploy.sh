#!/bin/bash

TOMCAT_DIR=/home/davidpg/APPS//tomcat_9_0_30
PROJECT_DIR=/home/davidpg/workspace/java/RaDiOS-MTT

cp $PROJECT_DIR/src/main/webapp/index.html $TOMCAT_DIR/webapps/RaDiOS-MTT/
cp -R $PROJECT_DIR/src/main/webapp/css/* $TOMCAT_DIR/webapps/RaDiOS-MTT/css/
cp -R $PROJECT_DIR/src/main/webapp/js/* $TOMCAT_DIR/webapps/RaDiOS-MTT/js
