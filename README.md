## RaDiOS-MTT
**Proyecto desarrollado para mi tesis doctoral y cuya función principal es la generación automática de modelos de simulación como soporte a la toma de decisiones en evaluaciones de tecnologías sanitarias.**

## Requisitos

Para la ejecución de la aplicación es necesario un contenedor de *servlets* tipo *Tomcat*, *Jetty*, etc., o un servidor de aplicaciones tipo *Wildfly*, *Jonas*, etc.

## Compilar el proyecto

    mvn clean package -Dmaven.test.skip=true

## Ejecución (*Linux*)
En el directorio raíz del proyecto existe un fichero llamado **run.sh**. Este fichero está preparado para ejecutar la aplicación en un servidor *Tomcat 9.0* y antes de ejecutarlo, será necesario revisar primero las variables que harán que se ejecute correctamente:

    TOMCAT_DIR=/home/davidpg/APPS//tomcat_9_0_30
    PROJECT_DIR=/home/davidpg/workspace/java/RaDiOS-MTT

## Generación de librería para incluir en PSIGHOS
Dentro del proyecto existe un fichero llamado **export2Psighos.jardesc**. Para reconfigurarlo sólo es necesario hacer doble *click* sobre el desde el *Eclipse* y se abrirá un *wizard* que permite reconfigurar los paquetes a exportar y la ruta destino del **.jar** resultante.

## Añadir una nueva propiedad en RADIOS-MTT
*RaDiOS-MTT* es una aplicación cuya base de conocimientos es una ontología, así que será necesario estar familiarizado con el lenguaje *OWL2*.

Los pasos a grandes rasgos conllevan lo siguiente:

 1. Modificar RaDiOS para agregar el nuevo diseño. La modificación puede ser todo lo compleja que se quiera, desde añadir una DataProperty hasta una modificación completa compuesta por Classes, ObjectProperties y DataProperties.
En esta versión de RaDiOS-MTT por simplicidad, se ha creado un fichero de ontología con sus instancias por separado. La idea final es la de tener un solo fichero **.owl** con la definición de RaDiOS y sus instancias para todas las enfermedades definidas.
Por ejemplo, para RaDiOS con las instancias de Definiencia de Biotidinasa se tiene este fichero:
    **/RaDiOS-MTT/src/main/resources/owl/radios_PBD.owl**
 2. Para poder propagar el nuevo diseño en Psighos será necesario también modificar el fichero:
    **/RaDiOS-MTT/src/main/resources/json/schema_4_simulation.json**
Este fichero es un fichero de esquema *Json* que simplifica mucho la lectura en texto de la información extraída del lenguaje estándar *OWL*.
Para generar las clases *Java* necesarias para trabajar con el nuevo diseño será necesario generar los fuentes con *Maven*. Desde la línea de comandos, en la raíz del proyecto, sería con la siguiente sentencia:
    **mvn generate-sources**
Esto generará las clases *Java* en el paquete/carpeta:
    **/RaDiOS-MTT/target/generated-sources/json-sources/es/ull/iis/ontology/radios/json/schema4simulation**

## *Url* de acceso
    http://localhost:8080/RaDiOS-MTT/index.html
