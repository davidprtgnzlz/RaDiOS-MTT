#!/bin/bash

TOMCAT_DIR=/home/davidpg/APPS//tomcat_9_0_30
PROJECT_DIR=/home/davidpg/workspace/java/RaDiOS-MTT

$TOMCAT_DIR/bin/catalina.sh stop
cd $PROJECT_DIR
mvn clean package -Dmaven.test.skip=true
rm -rf $TOMCAT_DIR/webapps/RaDiOS-MT*
cp $PROJECT_DIR/target/RaDiOS-MTT.war $TOMCAT_DIR/webapps/
$TOMCAT_DIR/bin/catalina.sh jpda run
